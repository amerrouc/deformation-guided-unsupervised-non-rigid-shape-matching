import torch.nn as nn
from torch.nn import functional as F
import torch
import torch_geometric
from torch.autograd import Variable
import numpy as np

# in batches 0 is for target shape, 1 is for source shape (1 is being deformed into 0)
def pool(x, batch, cluster, mode='max'):
    def merge(r_1, b_1, r_2, b_2):
        return torch.cat([r_1, r_2], dim=0), torch.cat([b_1, b_2])
    if cluster is None:
        # standard max pool
        if mode=='max':
            return torch_geometric.nn.global_max_pool(x, batch, size=2)
        if mode=='mean':
            return torch_geometric.nn.global_mean_pool(x, batch, size=2)
        if mode=='pass':
            return x
    else:
        # pool cluster wise
        if mode=='max':
            f = torch_geometric.nn.max_pool_x
        if mode=='mean':
            f = torch_geometric.nn.avg_pool_x
        r_1, b_1 = f(cluster[batch == 0], x[batch == 0], batch[batch == 0])
        r_2, b_2 = f(cluster[batch == 1], x[batch == 1], batch[batch == 1])
        return merge(r_1, b_1, r_2, b_2)

def unpool(cluster, x, v_batch, c_batch):
    if cluster is None:
        return x
    else:
        r_1 = torch.nn.functional.embedding(cluster[v_batch == 0], x[c_batch == 0])
        r_2 = torch.nn.functional.embedding(cluster[v_batch == 1], x[c_batch == 1])
        return torch.cat([r_1, r_2], dim=0)

def rotation_6d_to_matrix(d6):
    a1, a2 = d6[..., :3], d6[..., 3:]
    b1 = F.normalize(a1, dim=-1)
    b2 = a2 - (b1 * a2).sum(-1, keepdim=True) * b1
    b2 = F.normalize(b2, dim=-1)
    b3 = torch.cross(b1, b2, dim=-1)
    return torch.stack((b1, b2, b3), dim=-2)

class MLP(nn.Module):
    def __init__(self, input_dim, output_dim, hidden_dims = [], bias = True, act = nn.ELU()):
        super().__init__()
        self.input_dim = input_dim
        self.output_dim = output_dim
        self.hidden_dims = hidden_dims
        self.act = act
        if len(hidden_dims)>0:
            fc = [nn.Linear(input_dim, self.hidden_dims[0], bias = bias)]
            fc.append(act)
            for i in range(len(self.hidden_dims)-1):
                fc.append(nn.Linear(self.hidden_dims[i], self.hidden_dims[i+1], bias = bias))
                fc.append(act)
            fc.append(nn.Linear(self.hidden_dims[-1], output_dim, bias = bias))
        else:
            fc = [nn.Linear(input_dim, output_dim, bias = bias), act]
        self.linear = nn.Sequential(*fc)
    
    def forward(self, x):
        return self.linear(x)

class ConvBlock(nn.Module):
    def __init__(self, input_dim, output_dim, hidden_dims = [], conv = torch_geometric.nn.FeaStConv, heads = 9, bias = True, act = nn.ELU()):
        super().__init__()
        
        self.input_dim = input_dim
        self.output_dim = output_dim
        self.hidden_dims = hidden_dims
        self.act = act
        self.bias = bias
        self.conv = conv
        self.heads = heads
        self.conv_layers = self.build_conv_layers(input_dim, hidden_dims, output_dim, heads, bias)
    def build_conv_layers(self, input_dim, hidden_dims, output_dim, heads, bias):
        conv_layers = nn.ModuleList()
        for dim in hidden_dims:
            conv_layers.append(self.conv(in_channels=input_dim, out_channels=dim, heads=heads, add_self_loops=True, bias = bias))
            input_dim = dim
        conv_layers.append(self.conv(in_channels=input_dim, out_channels=output_dim, heads=heads, add_self_loops=True, bias = bias))
        return conv_layers
    def forward_conv_layers(self, x, adj):
        for conv_layer in self.conv_layers:
            x = conv_layer(x, adj)
            x = self.act(x)
        return x

    def forward(self, x, adj):
        x = self.forward_conv_layers(x, adj)
        return x

class RegionEncoderConvV(nn.Module):
    def __init__(self, input_dim, hidden_dims = [[32],[64],[128],[256]], heads = [9,9,9,9], bias = True, act = nn.ELU()):
        super().__init__()
        self.input_dim = input_dim
        self.hidden_dims = hidden_dims
        self.bias = bias
        self.act = act
        self.heads = heads
        self.spatial_encoder = nn.Linear(input_dim, 16)
        self.enc_layers = self.build_conv_blocks(16, hidden_dims, bias, heads, act)


    def build_conv_blocks(self, input_dim, hidden_dims, bias, heads, act):
        updown_enc_blocks = nn.ModuleList()
        for i in range(len(hidden_dims)):
            updown_enc_blocks.append(ConvBlock(input_dim = input_dim, output_dim = hidden_dims[i][-1], hidden_dims = hidden_dims[i][:-1], conv = torch_geometric.nn.FeaStConv, heads = heads[i], bias = bias, act = act))
            input_dim = hidden_dims[i][-1]
        return updown_enc_blocks
    def unpool_pool(self, x, c_batch, v_batch, previous_cluster, current_cluster, mode = 'max'):
        # from finer cluster level to vertex level
        x = unpool(cluster = previous_cluster, x = x, v_batch = v_batch, c_batch = c_batch)
        # from vertex level to coarser cluster level 
        x, c_batch = pool(x = x, batch = v_batch, cluster = current_cluster, mode=mode)
        return x, c_batch
    def build_adj(self, adj_target, adj_source):
        return torch.cat([adj_target, adj_source+adj_target.max().item()+1], dim=-1)
    def forward(self, x, v_batch, adjs_source, adjs_target, clusters, mode = 'max'):
        x = self.act(self.spatial_encoder(x))
        # first conv (at vertex level)
        x = self.enc_layers[0](x, self.build_adj(adj_target = adjs_target[0], adj_source = adjs_source[0]))
        previous_cluster = None
        c_batch = torch.clone(v_batch)
        features = [(x, c_batch)]
        for i in range(1,len(self.enc_layers)):
            # first step is to unpool pool
            x, c_batch = self.unpool_pool(x=x, c_batch=c_batch, v_batch=v_batch, previous_cluster=previous_cluster, current_cluster=clusters[i-1], mode = mode)
            x = self.enc_layers[i](x, self.build_adj(adj_target = adjs_target[i], adj_source = adjs_source[i]))
            previous_cluster = clusters[i-1]
            features.append((x, c_batch))
        return features





### warning new model with corrected deformation model
class DeformationDecoderLocal(nn.Module):
    def __init__(self, hidden_dims = [1024,512,256], heads = 9, bias = True, act = nn.ELU()):
        super().__init__()
        self.spatial_anchor_network = RegionEncoderConvV(input_dim = 6, hidden_dims = hidden_dims, heads = heads, bias = bias, act = act)
        self.pre_deformation_decoder_layer = ConvBlock(input_dim = 2*hidden_dims[-1][-1]+2*6, output_dim = hidden_dims[-1][-1], hidden_dims = [], conv = torch_geometric.nn.FeaStConv, heads = heads[-1], bias = True, act = act)
        self.deformation_decoder_layer = MLP(input_dim = hidden_dims[-1][-1], output_dim = 3+6, hidden_dims = [1024,512,256], bias = bias, act = act)            

    def gather_neighbours(self, x, list_adj):
        x = torch.index_select(x, 0, list_adj.reshape(-1))
        x = x.view(list_adj.shape[0], list_adj.shape[-1], x.shape[-1])
        return x

    def build_adj(self, adj_target, adj_source):
        return torch.cat([adj_target, adj_source+adj_target.max().item()+1], dim=-1)

    def forward(self, Pi_s_t, Pi_t_s, clusters, v_batch, list_adj_source, list_adj_target, original_source, original_source_normals, original_target,\
        original_target_normals, clusters_all, adj_s, adj_t, point_cluster_weights_s, point_cluster_weights_t, source_node_ids, target_node_ids, adjs_s, adjs_t, mode):

        # getting spatial anchors for patches undergoing deformatons
        anchors = self.spatial_anchor_network(x = torch.cat([torch.cat([original_target, original_target_normals], dim = -1), torch.cat([original_source, original_source_normals], dim = -1)], dim = 0), v_batch = v_batch,\
        adjs_source = adjs_s, adjs_target = adjs_t, clusters = clusters_all, mode=mode)
        anchors, c_batch = anchors[-1]

        original_cluster_centers_source = original_source[source_node_ids]
        original_cluster_normals_source = original_source_normals[source_node_ids]
        original_cluster_centers_target = original_target[target_node_ids]
        original_cluster_normals_target = original_target_normals[target_node_ids]
        ## stopped here
        original_anchors_target = anchors[c_batch == 0]
        original_anchors_source = anchors[c_batch == 1]
        # projecting features, to be fed to the deformation decoder
        # def : source to target
        s_projected_in_t_sspace = Pi_s_t @ torch.cat([original_cluster_centers_target, original_cluster_normals_target], dim=-1)
        s_projected_in_t_fspace = Pi_s_t @ original_anchors_target
        s_t_input = torch.cat([torch.cat([original_anchors_source, original_cluster_centers_source, original_cluster_normals_source], dim=-1), s_projected_in_t_fspace ,s_projected_in_t_sspace], dim=-1)
        # def : target to source
        t_projected_in_s_sspace = Pi_t_s @ torch.cat([original_cluster_centers_source, original_cluster_normals_source], dim=-1)
        t_projected_in_s_fspace = Pi_t_s @ original_anchors_source
        t_s_input = torch.cat([torch.cat([original_anchors_target, original_cluster_centers_target, original_cluster_normals_target], dim=-1), t_projected_in_s_fspace, t_projected_in_s_sspace], dim=-1)


        # produce deformations for source to target and target to source
        pre_deformations = self.pre_deformation_decoder_layer(x = torch.cat([t_s_input, s_t_input], dim=0), adj = self.build_adj(adj_target=adj_t, adj_source=adj_s))
        deformations = self.deformation_decoder_layer(pre_deformations)
        rotations = deformations[:,3:]
        iden = Variable(torch.from_numpy(np.array([1,0,0,0,1,0]).astype(np.float32))).view(1,6).repeat(rotations.shape[0],1)
        if rotations.is_cuda:
            iden = iden.cuda()
        rotations = rotations + iden
        offsets = deformations[:,:3]


        # execute source to target deformation
        offsets_s_t = offsets[c_batch == 1]
        rotations_s_t = rotations[c_batch == 1]
        rotations_s_t = rotation_6d_to_matrix(rotations_s_t)
        self.rotations_s_t = rotations_s_t
        rotations_s_t = rotations_s_t.view(-1, 9)
        transformed_source, transformed_source_normals, rigidity_s_t = self.transform_shape(source = original_source, source_normals=original_source_normals, offsets = offsets_s_t, rotations = rotations_s_t, \
            cluster_source = clusters[v_batch == 1], list_adj_source = list_adj_source, source_node_ids = source_node_ids, point_cluster_weights_s = point_cluster_weights_s)

        # execute target to source deformation
        offsets_t_s = offsets[c_batch == 0]
        rotations_t_s = rotations[c_batch == 0]
        rotations_t_s = rotation_6d_to_matrix(rotations_t_s)
        self.rotations_t_s = rotations_t_s
        rotations_t_s = rotations_t_s.view(-1, 9)
        transformed_target, transformed_target_normals, rigidity_t_s = self.transform_shape(source = original_target, source_normals=original_target_normals, offsets = offsets_t_s, rotations = rotations_t_s, \
            cluster_source = clusters[v_batch == 0], list_adj_source = list_adj_target, source_node_ids = target_node_ids, point_cluster_weights_s = point_cluster_weights_t)

        self.rigidity_loss = rigidity_s_t+rigidity_t_s
        return transformed_source, transformed_source_normals, transformed_target, transformed_target_normals
    def transform_shape(self, source, source_normals, offsets, rotations, cluster_source, list_adj_source, source_node_ids, point_cluster_weights_s):
        # get adj as list (intermediaryadjs are with no self loops)
        adj_list, mask = list_adj_source
        # gathered offsets, rotations, centers and original centers
        gathered_offsets = self.gather_neighbours(offsets, adj_list)
        gathered_rotations = self.gather_neighbours(rotations, adj_list)
        gathered_centers = self.gather_neighbours(source[source_node_ids], adj_list)
        mask = mask[cluster_source]

        # compute prediction for each cluster

        # relative corrdinate frame (only change of origin,  directionl quantities such as normals not affected)

        transformed_source = source.unsqueeze(-2) - gathered_centers[cluster_source]
        # rotate patches and normals
        gathered_rotations = gathered_rotations[cluster_source].reshape(transformed_source.shape[0],transformed_source.shape[1], 3, 3)
        transformed_source = torch.matmul(gathered_rotations, transformed_source.unsqueeze(-1)).squeeze(-1)
        # directional quantity not dependent on change of origin
        transformed_source_normals = torch.matmul(gathered_rotations, source_normals.unsqueeze(-2).repeat(1,gathered_rotations.shape[1],1).unsqueeze(-1)).squeeze(-1)
        # translate
        transformed_source = transformed_source + gathered_offsets[cluster_source]

        # go back to world coordinates
        transformed_source = transformed_source + gathered_centers[cluster_source]
        weights = point_cluster_weights_s


        # compute rigidity loss
        rigidity_loss = 0.0
        K = weights.shape[1]
        indices = torch.stack([torch.zeros(K-1), torch.arange(K-1)+1], dim=-1).long().cuda()
        v_1, v_2 = transformed_source[:,indices[:,0],:], transformed_source[:,indices[:,1],:]
        w_1, w_2 = weights[:,indices[:,0]], weights[:,indices[:,1]]
        mask_loss = mask[:,indices[:,0]]+mask[:,indices[:,1]]
        w = (w_1+w_2).masked_fill(mask_loss, 0.0)
        loss = w*(((v_1-v_2)**2).sum(dim=-1))
        rigidity_loss = loss.mean(dim=-1).mean()
        transformed_source = (weights.unsqueeze(-1) * transformed_source).sum(dim=-2)
        transformed_source_normals = (weights.unsqueeze(-1) * transformed_source_normals).sum(dim=-2)
        return transformed_source, transformed_source_normals, rigidity_loss

class DeformationDecoderVLocal(nn.Module):
    def __init__(self, hidden_dims = [1024,512,256], heads = 9, bias = True, act = nn.ELU()):
        super().__init__()
        self.spatial_anchor_network = RegionEncoderConvV(input_dim = 6, hidden_dims = hidden_dims, heads = heads, bias = bias, act = act)
        self.pre_deformation_decoder_layer = ConvBlock(input_dim = 2*hidden_dims[-1][-1]+2*6, output_dim = hidden_dims[-1][-1], hidden_dims = [], conv = torch_geometric.nn.FeaStConv, heads = heads[-1], bias = True, act = act)
        self.deformation_decoder_layer = MLP(input_dim = hidden_dims[-1][-1], output_dim = 3+6, hidden_dims = [1024,512,256], bias = bias, act = act)            

    def gather_neighbours(self, x, list_adj):
        x = torch.index_select(x, 0, list_adj.reshape(-1))
        x = x.view(list_adj.shape[0], list_adj.shape[-1], x.shape[-1])
        return x
    def build_adj(self, adj_target, adj_source):
        return torch.cat([adj_target, adj_source+adj_target.max().item()+1], dim=-1)
    def forward(self, Pi_s_t, Pi_t_s, v_batch, list_adj_source, list_adj_target, original_source, original_source_normals, original_target,\
        original_target_normals, clusters_all, adj_s, adj_t, point_cluster_weights_s, point_cluster_weights_t, adjs_s, adjs_t, mode):        

        # getting spatial anchors for patches undergoing deformatons
        anchors = self.spatial_anchor_network(x = torch.cat([torch.cat([original_target, original_target_normals], dim = -1), torch.cat([original_source, original_source_normals], dim = -1)], dim = 0), v_batch = v_batch,\
        adjs_source = adjs_s, adjs_target = adjs_t, clusters = clusters_all, mode = mode)
        anchors, c_batch = anchors[-1]

        assert((c_batch == v_batch).all())

        original_cluster_centers_source = original_source
        original_cluster_normals_source = original_source_normals
        original_cluster_centers_target = original_target
        original_cluster_normals_target = original_target_normals

        original_anchors_target = anchors[c_batch == 0]
        original_anchors_source = anchors[c_batch == 1]
        # projecting features, to be fed to the deformation decoder
        # def : source to target
        s_projected_in_t_sspace = Pi_s_t @ torch.cat([original_cluster_centers_target, original_cluster_normals_target], dim=-1)
        s_projected_in_t_fspace = Pi_s_t @ original_anchors_target
        s_t_input = torch.cat([torch.cat([original_anchors_source, original_cluster_centers_source, original_cluster_normals_source], dim=-1), s_projected_in_t_fspace ,s_projected_in_t_sspace], dim=-1)
        # def : target to source
        t_projected_in_s_sspace = Pi_t_s @ torch.cat([original_cluster_centers_source, original_cluster_normals_source], dim=-1)
        t_projected_in_s_fspace = Pi_t_s @ original_anchors_source
        t_s_input = torch.cat([torch.cat([original_anchors_target, original_cluster_centers_target, original_cluster_normals_target], dim=-1), t_projected_in_s_fspace, t_projected_in_s_sspace], dim=-1)


        # produce deformations for source to target and target to source
        pre_deformations = self.pre_deformation_decoder_layer(x = torch.cat([t_s_input, s_t_input], dim=0), adj = self.build_adj(adj_target=adj_t, adj_source=adj_s))
        deformations = self.deformation_decoder_layer(pre_deformations)
        rotations = deformations[:,3:]
        iden = Variable(torch.from_numpy(np.array([1,0,0,0,1,0]).astype(np.float32))).view(1,6).repeat(rotations.shape[0],1)
        if rotations.is_cuda:
            iden = iden.cuda()
        rotations = rotations + iden
        offsets = deformations[:,:3]


        # execute source to target deformation
        offsets_s_t = offsets[c_batch == 1]
        rotations_s_t = rotations[c_batch == 1]
        rotations_s_t = rotation_6d_to_matrix(rotations_s_t)
        self.rotations_s_t = rotations_s_t
        rotations_s_t = rotations_s_t.view(-1, 9)
        transformed_source, transformed_source_normals, rigidity_s_t = self.transform_shape(source = original_source, source_normals=original_source_normals, offsets = offsets_s_t, rotations = rotations_s_t, \
            list_adj_source = list_adj_source, point_cluster_weights_s = point_cluster_weights_s)

        # execute target to source deformation
        offsets_t_s = offsets[c_batch == 0]
        rotations_t_s = rotations[c_batch == 0]
        rotations_t_s = rotation_6d_to_matrix(rotations_t_s)
        self.rotations_t_s = rotations_t_s
        rotations_t_s = rotations_t_s.view(-1, 9)
        transformed_target, transformed_target_normals, rigidity_t_s = self.transform_shape(source = original_target, source_normals=original_target_normals, offsets = offsets_t_s, rotations = rotations_t_s, \
            list_adj_source = list_adj_target, point_cluster_weights_s = point_cluster_weights_t)

        self.rigidity_loss = rigidity_s_t+rigidity_t_s
        return transformed_source, transformed_source_normals, transformed_target, transformed_target_normals
    def transform_shape(self, source, source_normals, offsets, rotations, list_adj_source, point_cluster_weights_s):
        # get adj as list (intermediaryadjs are with no self loops)
        adj_list, mask = list_adj_source
        # gathered offsets, rotations, centers and original centers
        gathered_offsets = self.gather_neighbours(offsets, adj_list)
        gathered_rotations = self.gather_neighbours(rotations, adj_list)
        gathered_centers = self.gather_neighbours(source, adj_list)
        mask = mask
        # compute prediction for each cluster

        # relative corrdinate frame
        transformed_source = source.unsqueeze(-2) - gathered_centers
        # rotate patches and normals
        gathered_rotations = gathered_rotations.reshape(transformed_source.shape[0],transformed_source.shape[1], 3, 3)
        transformed_source = torch.matmul(gathered_rotations, transformed_source.unsqueeze(-1)).squeeze(-1)
        transformed_source_normals = torch.matmul(gathered_rotations, source_normals.unsqueeze(-2).repeat(1,gathered_rotations.shape[1],1).unsqueeze(-1)).squeeze(-1)
        # translate
        transformed_source = transformed_source + gathered_offsets
        # back to world coordinates
        transformed_source = transformed_source + gathered_centers
        weights = point_cluster_weights_s


        # compute rigidity loss
        rigidity_loss = 0.0
        K = weights.shape[1]
        indices = torch.stack([torch.zeros(K-1), torch.arange(K-1)+1], dim=-1).long().cuda()
        v_1, v_2 = transformed_source[:,indices[:,0],:], transformed_source[:,indices[:,1],:]
        w_1, w_2 = weights[:,indices[:,0]], weights[:,indices[:,1]]
        mask_loss = mask[:,indices[:,0]]+mask[:,indices[:,1]]
        w = (w_1+w_2).masked_fill(mask_loss, 0.0)
        loss = w*(((v_1-v_2)**2).sum(dim=-1))
        rigidity_loss = loss.mean(dim=-1).mean()
        transformed_source = (weights.unsqueeze(-1) * transformed_source).sum(dim=-2)
        transformed_source_normals = (weights.unsqueeze(-1) * transformed_source_normals).sum(dim=-2)
        return transformed_source, transformed_source_normals, rigidity_loss

class DeformationNetworkLocal(nn.Module):
    def __init__(self, input_dim, deformation_hidden_dims = [[32],[64],[128],[256]], association_hidden_dims = None, heads = [9,9,9,9], bias = True, act = nn.ELU()):
        super().__init__()
        # offset decoders
        self.offset_finders = self.build_offset_layers(deformation_hidden_dims, heads, bias, act)
    def build_offset_layers(self, hidden_dims, heads, bias, act):
        # first is node level, last is shape level
        offset_finders = nn.ModuleList()
        for i in range(len(hidden_dims)-1):
            offset_finders.append(DeformationDecoderLocal(hidden_dims = hidden_dims[0:len(hidden_dims)-i], heads = heads[0:len(hidden_dims)-i], bias = bias, act = act))
        i = i + 1
        offset_finders.append(DeformationDecoderVLocal(hidden_dims = hidden_dims[0:len(hidden_dims)-i], heads = heads[0:len(hidden_dims)-i], bias = bias, act = act))
        return offset_finders
    def forward(self, pos_normals_s, clusters_s, adjs_s, list_adjs_s, mapping_s, cluster_sizes_s, distance_matrices_s,\
        pos_normals_t, clusters_t, adjs_t, list_adjs_t, mapping_t, cluster_sizes_t, distance_matrices_t, v_batch, point_cluster_weights_s, point_cluster_weights_t, Pis_s, mode='max'):
        # getting source and target shapes
        source_shape = pos_normals_s[:,:3]
        source_shape_normals = pos_normals_s[:,3:]
        target_shape = pos_normals_t[:,:3]
        target_shape_normals = pos_normals_t[:,3:]

        # fine to coarse
        clusters = [torch.cat([clusters_t[k+1], clusters_s[k+1]], dim=-1) for k in range(len(cluster_sizes_s.keys()))]

        # starting deformation procedure (simultaneously deform source into target and target into source)

        transformed_source = source_shape
        transformed_source_normals = source_shape_normals
        transformed_target = target_shape
        transformed_target_normals = target_shape_normals

        self.shapes = [[], []]

        self.offset_rigidity_losses = []
        self.offset_matching_losses = []
        # coarse to fine
        reversed_clusters = list(reversed(clusters))
        for i, (Pis) in enumerate(Pis_s[:-1]):

            Pi_s_s, Pi_s_t, Pi_t_t, Pi_t_s = Pis

            idx_dict = len(clusters)-i

            source_node_ids = mapping_s[torch.unique(reversed_clusters[i][v_batch == 1])]
            target_node_ids = mapping_t[torch.unique(reversed_clusters[i][v_batch == 0])]
            transformed_source, transformed_source_normals, transformed_target, transformed_target_normals = self.offset_finders[i](Pi_s_t = Pi_s_t, Pi_t_s = Pi_t_s, clusters = reversed_clusters[i], v_batch = v_batch,\
            list_adj_source = list_adjs_s[idx_dict], list_adj_target = list_adjs_t[idx_dict], original_source = source_shape, original_source_normals = source_shape_normals, original_target = target_shape,\
            original_target_normals = target_shape_normals, clusters_all = clusters, adj_s=adjs_s[idx_dict], adj_t=adjs_t[idx_dict], point_cluster_weights_s = point_cluster_weights_s[idx_dict],\
            point_cluster_weights_t = point_cluster_weights_t[idx_dict], source_node_ids = source_node_ids, target_node_ids = target_node_ids, adjs_s = adjs_s, adjs_t = adjs_t, mode = mode)            
    
            self.shapes[0].append(transformed_source)
            self.shapes[1].append(transformed_target)

        i += 1
        idx_dict = len(clusters)-i
        Pi_s_s, Pi_s_t, Pi_t_t, Pi_t_s = Pis_s[i]
        transformed_source, transformed_source_normals, transformed_target, transformed_target_normals = self.offset_finders[i](Pi_s_t = Pi_s_t, Pi_t_s = Pi_t_s, v_batch = v_batch,\
        list_adj_source = list_adjs_s[idx_dict], list_adj_target = list_adjs_t[idx_dict], original_source = source_shape, original_source_normals = source_shape_normals, original_target = target_shape,\
        original_target_normals = target_shape_normals, clusters_all = clusters, adj_s=adjs_s[idx_dict], adj_t=adjs_t[idx_dict], point_cluster_weights_s = point_cluster_weights_s[idx_dict],\
        point_cluster_weights_t = point_cluster_weights_t[idx_dict], adjs_s = adjs_s, adjs_t = adjs_t, mode = mode)


        self.shapes[0].append(transformed_source)
        self.shapes[1].append(transformed_target)

        return self.shapes
    def get_matching_loss(self, original_target, original_source, target_node_ids, source_node_ids, transformed_target, transformed_source, Pi_s_t, Pi_t_s):

        if target_node_ids is not None:
            orig_tau_tt = original_target[target_node_ids]
            orig_rau_ss = original_source[source_node_ids]
            rau_tt_def = transformed_source[source_node_ids]
            tau_ss_def = transformed_target[target_node_ids]
        else:
            orig_tau_tt = original_target
            orig_rau_ss = original_source
            rau_tt_def = transformed_source
            tau_ss_def = transformed_target
        rau_tt_targ = Pi_s_t @ orig_tau_tt
        tau_ss_targ = Pi_t_s @ orig_rau_ss


        rau_tt_targ = Pi_s_t @ orig_tau_tt
        tau_ss_targ = Pi_t_s @ orig_rau_ss

        matching_loss = ((rau_tt_targ - rau_tt_def)**2).mean() + ((tau_ss_targ - tau_ss_def)**2).mean()

        return matching_loss
    def get_matching_losses(self, mapping_s, mapping_t, reveresed_clusters, v_batch, original_target, original_source, Pis, weights):
        matching_losses = []
        assert(len(Pis) == len(weights[0]))
        assert(len(weights[1]) == 1)
        for i in range(len(Pis)):
            Pi_s_s, Pi_s_t, Pi_t_t, Pi_t_s = Pis[i]
            transformed_source, transformed_target = self.shapes[0][i], self.shapes[1][i]
            if i < (len(Pis)-1):
                source_node_ids = mapping_s[torch.unique(reveresed_clusters[i][v_batch == 1])]
                target_node_ids = mapping_t[torch.unique(reveresed_clusters[i][v_batch == 0])]
            else:
                source_node_ids = None
                target_node_ids = None

            matching_loss = self.get_matching_loss(original_target = original_target, original_source = original_source, target_node_ids = target_node_ids, source_node_ids = source_node_ids,\
            transformed_target = transformed_target, transformed_source = transformed_source, Pi_s_t = Pi_s_t, Pi_t_s = Pi_t_s)
            matching_losses.append(weights[0][i]*matching_loss)
        return matching_losses
    def get_rigidity_loss(self, weights):
        rigidity_losses = []
        assert(len(self.offset_finders) == len(weights[0]))
        assert(len(weights[1]) == 1)
        for i in range(len(self.offset_finders)):
            rigidity_losses.append(weights[0][i]*self.offset_finders[i].rigidity_loss)
        return rigidity_losses



class SmoothedAssociationNetworkNoAct(nn.Module):
    def __init__(self, input_dim, association_hidden_dims = [[32],[64],[128],[256]], aggregation_hidden_dims = [[256, 256],[256, 256],[256, 256]], deformation_hidden_dims = None, heads = [9,9,9,9], bias = True, act = nn.ELU()):
        super().__init__()
        self.region_encoder = RegionEncoderConvV(input_dim = input_dim, hidden_dims = association_hidden_dims, heads = heads, bias = bias, act = act)
        self.correspondence_aggregators = self.build_correspondence_aggregators(association_hidden_dims = association_hidden_dims, aggregation_hidden_dims = aggregation_hidden_dims, bias = bias, heads = heads, act = act)
    def build_correspondence_aggregators(self, association_hidden_dims, aggregation_hidden_dims, bias, heads, act):
        downup_aggregators = nn.ModuleList()
        last_dim = association_hidden_dims[-1][-1]
        for i, hd in enumerate(list(reversed(association_hidden_dims[:-1]))):
            downup_aggregators.append(ConvBlock(input_dim = last_dim+hd[-1], output_dim = last_dim, hidden_dims = aggregation_hidden_dims[i], conv = torch_geometric.nn.FeaStConv, heads = heads[i], bias = bias, act = nn.Identity()))
        return downup_aggregators
    def get_bidirectional_correspondence_map(self, x_s, x_t, sigma):
        x_s = x_s / x_s.norm(dim=-1, keepdim=True)
        x_t = x_t / x_t.norm(dim=-1, keepdim=True)

        D_s_t = torch.mm(x_s, x_t.T)
        D_t_s = torch.mm(x_t, x_s.T)
        D_s_s = torch.mm(x_s, x_s.T)
        D_t_t = torch.mm(x_t, x_t.T)

        Pi_s_t = F.softmax(D_s_t * sigma, dim=-1)
        Pi_s_s = F.softmax(D_s_s * sigma, dim=-1)
        Pi_t_t = F.softmax(D_t_t * sigma, dim=-1)
        Pi_t_s = F.softmax(D_t_s * sigma, dim=-1)
        return Pi_s_s, Pi_s_t, Pi_t_t, Pi_t_s
    def unpool_pool(self, x, c_batch, v_batch, previous_cluster, current_cluster, mode = 'max'):
        # from finer cluster level to vertex level
        x = unpool(cluster = previous_cluster, x = x, v_batch = v_batch, c_batch = c_batch)
        # from vertex level to coarser cluster level 
        x, c_batch = pool(x = x, batch = v_batch, cluster = current_cluster, mode=mode)
        return x, c_batch
    def build_adj(self, adj_target, adj_source):
        return torch.cat([adj_target, adj_source+adj_target.max().item()+1], dim=-1)
    def forward(self, pos_normals_s, clusters_s, adjs_s, cluster_sizes_s, pos_normals_t, clusters_t, adjs_t, v_batch, sigmas = [1e2, 1e2, 1e2, 1e2], mode = 'max'):
    
        # build features and clusters (batches)
        x = torch.cat([pos_normals_t, pos_normals_s], dim=0)
        # fine to coarse
        clusters = [torch.cat([clusters_t[k+1], clusters_s[k+1]], dim=-1) for k in range(len(cluster_sizes_s.keys()))]

        # extracting features with convolutional multilevel network
        features_cbatch = self.region_encoder(x = x, v_batch = v_batch, adjs_source = adjs_s, adjs_target = adjs_t, clusters = clusters, mode = mode)


        # extracting the association matrices (from fine to coarse)
        reversed_clusters = list(reversed(clusters))
        reveresed_features_cbatch = list(reversed(features_cbatch))
        reversed_sigmas = list(reversed(sigmas))
        previous_features = None
        previous_features_warped = None
        previous_c_batch = None
        previous_clusters = None
        self.Pis = []
        for i, (features, c_batch) in enumerate(reveresed_features_cbatch):
            idx_dict = len(cluster_sizes_s.keys())-i
            # unpool
            if previous_features is None:
                previous_features = features
                previous_c_batch = c_batch
                previous_clusters = reversed_clusters[i]
                pss, pst, ptt, pts = self.get_bidirectional_correspondence_map(x_s = previous_features[c_batch == 1], x_t = previous_features[c_batch == 0], sigma=reversed_sigmas[i])
            else:
                if (i < len(reveresed_features_cbatch)-1):
                    unpooled_previous_f, unpooled_c_batch = self.unpool_pool(x = previous_features, c_batch = previous_c_batch, v_batch = v_batch, previous_cluster = previous_clusters, current_cluster = reversed_clusters[i], mode=mode)
                    assert((unpooled_c_batch == c_batch).all())
                    previous_features = self.correspondence_aggregators[i-1](torch.cat([unpooled_previous_f, features], dim=-1), self.build_adj(adj_target = adjs_t[idx_dict], adj_source = adjs_s[idx_dict]))
                    previous_c_batch = c_batch
                    previous_clusters = reversed_clusters[i]
                else:
                    unpooled_previous_f = unpool(cluster = previous_clusters, x = previous_features, v_batch = v_batch, c_batch = previous_c_batch)
                    previous_features = self.correspondence_aggregators[i-1](torch.cat([unpooled_previous_f, features], dim=-1), self.build_adj(adj_target = adjs_t[idx_dict], adj_source = adjs_s[idx_dict]))
                pss, pst, ptt, pts = self.get_bidirectional_correspondence_map(x_s = previous_features[c_batch == 1],x_t = previous_features[c_batch == 0], sigma=reversed_sigmas[i])
            self.Pis.append((pss, pst, ptt, pts))
        return self.Pis


    def get_geodesic_distance_loss(self, Pi_s_t, Pi_t_s, dist_mat_source, dist_mat_target):
        return ((((Pi_s_t @ dist_mat_target) @ Pi_s_t.T) - dist_mat_source)**2).mean() + ((((Pi_t_s @ dist_mat_source) @ Pi_t_s.T) - dist_mat_target)**2).mean() 
    def get_geodesic_distance_losses(self, dists_mat_source, dists_mat_target, weights):
        total_loss = []
        assert(len(self.Pis) == len(weights[0]))
        assert(len(weights[1]) == 1)
        for i in range(len(self.Pis)):
            if weights[0][i] != 0:
                _, Pi_s_t, _, Pi_t_s = self.Pis[i]
                idx_dict = len(self.Pis)- i - 1 # -1 for the  vertex level not represented in our dicts
                total_loss.append(weights[0][i] * self.get_geodesic_distance_loss(Pi_s_t = Pi_s_t, Pi_t_s = Pi_t_s, dist_mat_source = dists_mat_source[idx_dict], dist_mat_target = dists_mat_target[idx_dict]))
            else:
                total_loss.append(torch.tensor(0.0))
        return total_loss
    def get_map_loss(self, original_target, original_source, target_node_ids, source_node_ids, Pi_s_s, Pi_s_t, Pi_t_t, Pi_t_s, spatial_cycle_loss, spatial_self_reconstruction_loss):
        # cycle consistency loss
        if spatial_cycle_loss:
            if target_node_ids is not None:
                orig_tau_tt = original_target[target_node_ids]
                orig_rau_ss = original_source[source_node_ids]
            else:
                orig_tau_tt = original_target
                orig_rau_ss = original_source
        
            orig_tau_ss = Pi_t_s @ orig_rau_ss
            orig_rau_tt = Pi_s_t @ orig_tau_tt

            orig_rau_ss_rec = Pi_s_t @ orig_tau_ss
            orig_tau_tt_rec = Pi_t_s @ orig_rau_tt

            cycle_loss = ((orig_rau_ss - orig_rau_ss_rec)**2).mean() + ((orig_tau_tt - orig_tau_tt_rec)**2).mean()
        else:
            cycle_loss = ((Pi_s_t - Pi_t_s.T)**2).mean()

        

        # self reconstruction loss
        if spatial_self_reconstruction_loss:
            if target_node_ids is not None:
                orig_tau_tt = original_target[target_node_ids]
                orig_rau_ss = original_source[source_node_ids]
            else:
                orig_tau_tt = original_target
                orig_rau_ss = original_source

            rau_ss_rec_from_self = Pi_s_s @ orig_rau_ss
            tau_tt_rec_from_self = Pi_t_t @ orig_tau_tt

            reconstruction_loss = ((rau_ss_rec_from_self - orig_rau_ss)**2).mean() + ((tau_tt_rec_from_self - orig_tau_tt)**2).mean()
        else:
            iden_t_t = torch.eye(Pi_t_t.shape[0]).cuda()
            iden_s_s = torch.eye(Pi_s_s.shape[0]).cuda()


            reconstruction_loss = ((Pi_s_s  - iden_s_s)**2).mean() + ((Pi_t_t  - iden_t_t)**2).mean()

        return cycle_loss, reconstruction_loss 
    def get_map_losses(self, mapping_s, mapping_t, reveresed_clusters, v_batch, original_target, original_source, weights_cycle, weights_self_reconstruction, spatial_cycle_loss, spatial_self_reconstruction_loss):
        cycle_losses = []
        reconstruction_losses = []
        assert(len(self.Pis) == len(weights_cycle[0]))
        assert(len(weights_cycle[1]) == 1)
        assert(len(self.Pis) == len(weights_self_reconstruction[0]))
        assert(len(weights_self_reconstruction[1]) == 1)
        for i in range(len(self.Pis)):
            Pi_s_s, Pi_s_t, Pi_t_t, Pi_t_s = self.Pis[i]
            if i < (len(self.Pis)-1):
                source_node_ids = mapping_s[torch.unique(reveresed_clusters[i][v_batch == 1])]
                target_node_ids = mapping_t[torch.unique(reveresed_clusters[i][v_batch == 0])]
            else:
                source_node_ids = None
                target_node_ids = None

            cycle_loss, reconstruction_loss = self.get_map_loss(original_target = original_target, original_source = original_source, target_node_ids = target_node_ids, source_node_ids = source_node_ids,\
            Pi_s_s = Pi_s_s, Pi_s_t = Pi_s_t, Pi_t_t = Pi_t_t, Pi_t_s = Pi_t_s, spatial_cycle_loss = spatial_cycle_loss, spatial_self_reconstruction_loss = spatial_self_reconstruction_loss)
            cycle_losses.append(weights_cycle[0][i]*cycle_loss)
            reconstruction_losses.append(weights_self_reconstruction[0][i]*reconstruction_loss)
        return cycle_losses, reconstruction_losses


    def get_matching_loss(self, original_target, original_source, target_node_ids, source_node_ids, transformed_target, transformed_source, Pi_s_t, Pi_t_s):
        if target_node_ids is not None:
            orig_tau_tt = original_target[target_node_ids]
            orig_rau_ss = original_source[source_node_ids]
            rau_tt_def = transformed_source[source_node_ids]
            tau_ss_def = transformed_target[target_node_ids]
        else:
            orig_tau_tt = original_target
            orig_rau_ss = original_source
            rau_tt_def = transformed_source
            tau_ss_def = transformed_target
        rau_tt_targ = Pi_s_t @ orig_tau_tt
        tau_ss_targ = Pi_t_s @ orig_rau_ss

        matching_loss = ((rau_tt_targ - rau_tt_def)**2).mean() + ((tau_ss_targ - tau_ss_def)**2).mean()

        return matching_loss
    def get_matching_losses(self, mapping_s, mapping_t, reveresed_clusters, v_batch, original_target, original_source, deformed_shapes, weights):
        matching_losses = []
        assert(len(self.Pis) == len(weights[0]))
        assert(len(weights[1]) == 1)
        for i in range(len(self.Pis)):
            Pi_s_s, Pi_s_t, Pi_t_t, Pi_t_s = self.Pis[i]
            transformed_source, transformed_target = deformed_shapes[0][i], deformed_shapes[1][i]
            if i < (len(self.Pis)-1):
                source_node_ids = mapping_s[torch.unique(reveresed_clusters[i][v_batch == 1])]
                target_node_ids = mapping_t[torch.unique(reveresed_clusters[i][v_batch == 0])]
            else:
                source_node_ids = None
                target_node_ids = None

            matching_loss = self.get_matching_loss(original_target = original_target, original_source = original_source, target_node_ids = target_node_ids, source_node_ids = source_node_ids,\
            transformed_target = transformed_target, transformed_source = transformed_source, Pi_s_t = Pi_s_t, Pi_t_s = Pi_t_s)
            matching_losses.append(weights[0][i]*matching_loss)
        return matching_losses
    


class MtpadVSmoothedLocalNoAct(nn.Module):
    def __init__(self, input_dim, deformation_hidden_dims = [[32],[64],[128],[256]], association_hidden_dims = None, aggregation_hidden_dims = None, heads = [9,9,9,9,9], bias = True, act = nn.ELU()):
        super().__init__()
        self.association_network = SmoothedAssociationNetworkNoAct(input_dim = input_dim, association_hidden_dims = association_hidden_dims, aggregation_hidden_dims = aggregation_hidden_dims, heads = heads, bias = bias, act = act)
        self.deformation_network = DeformationNetworkLocal(input_dim = input_dim, deformation_hidden_dims = deformation_hidden_dims, heads = heads, bias = bias, act = act)
    def forward(self, pos_normals_s, clusters_s, adjs_s, list_adjs_s, mapping_s, cluster_sizes_s, distance_matrices_s,\
        pos_normals_t, clusters_t, adjs_t, list_adjs_t, mapping_t, cluster_sizes_t, distance_matrices_t, v_batch, point_cluster_weights_s, point_cluster_weights_t, mode='max', sigmas = [1e2, 1e2, 1e2, 1e2]):
        # get the associations
        Pis = self.association_network(pos_normals_s = pos_normals_s, clusters_s = clusters_s, adjs_s = adjs_s, cluster_sizes_s = cluster_sizes_s, pos_normals_t = pos_normals_t, clusters_t = clusters_t, adjs_t = adjs_t,\
        v_batch = v_batch, sigmas = sigmas, mode = mode)
        # get the corresponding deformations
        shapes = self.deformation_network(pos_normals_s = pos_normals_s, clusters_s = clusters_s, adjs_s = adjs_s, list_adjs_s = list_adjs_s, mapping_s = mapping_s, cluster_sizes_s = clusters_s,\
        distance_matrices_s = distance_matrices_s, pos_normals_t = pos_normals_t, clusters_t = clusters_t, adjs_t = adjs_t, list_adjs_t = list_adjs_t, mapping_t = mapping_t, cluster_sizes_t = cluster_sizes_t,\
        distance_matrices_t = distance_matrices_t, v_batch = v_batch, point_cluster_weights_s = point_cluster_weights_s, point_cluster_weights_t = point_cluster_weights_t, Pis_s = Pis, mode=mode)
        return Pis, shapes

    def get_loss(self, shapes, Pis, pos_normals_s, pos_normals_t, clusters_s, clusters_t, cluster_sizes_s, distance_matrices_s, distance_matrices_t, mapping_s, mapping_t, v_batch,\
        weights_geodesic_distortion_loss = None, weights_cycle_loss = None, weights_self_reconstruction_loss = None, weights_association_matching_loss = None,\
        weights_deformation_rigidity_loss = None, weights_deformation_matching_loss = None, spatial_cycle_loss = True, spatial_self_reconstruction_loss = True,\
        association_loss = True, deformation_loss = True):

        reveresed_clusters = list(reversed([torch.cat([clusters_t[k+1], clusters_s[k+1]], dim=-1) for k in range(len(cluster_sizes_s.keys()))]))
        if association_loss:
        # getting the losses on the association part
            geodesic_assos_losses = self.association_network.get_geodesic_distance_losses(dists_mat_source = distance_matrices_s, dists_mat_target = distance_matrices_t, weights = weights_geodesic_distortion_loss)
            cycle_assos_losses, reconstruction_assos_losses = self.association_network.get_map_losses(mapping_s = mapping_s, mapping_t = mapping_t, reveresed_clusters = reveresed_clusters, v_batch = v_batch,\
            original_target = pos_normals_t[:,:3], original_source = pos_normals_s[:,:3], weights_cycle = weights_cycle_loss, weights_self_reconstruction = weights_self_reconstruction_loss,\
            spatial_cycle_loss = spatial_cycle_loss, spatial_self_reconstruction_loss = spatial_self_reconstruction_loss)
            matching_assos_losses = self.association_network.get_matching_losses(mapping_s = mapping_s, mapping_t = mapping_t, reveresed_clusters = reveresed_clusters, v_batch = v_batch, original_target = pos_normals_t[:,:3], original_source = pos_normals_s[:,:3],\
            deformed_shapes = shapes, weights = weights_association_matching_loss)

            geo_assos_loss = weights_geodesic_distortion_loss[1][0]*(sum(geodesic_assos_losses) / len(geodesic_assos_losses))
            match_assos_loss =  weights_association_matching_loss[1][0]*(sum(matching_assos_losses) / len(matching_assos_losses))
            cycle_assos_loss = weights_cycle_loss[1][0]*(sum(cycle_assos_losses) / len(cycle_assos_losses)) 
            self_reconstruction_assos_loss = weights_self_reconstruction_loss[1][0]*(sum(reconstruction_assos_losses) / len(reconstruction_assos_losses))

            total_association_loss = self_reconstruction_assos_loss + cycle_assos_loss + geo_assos_loss + match_assos_loss
        if deformation_loss:
            # getting the losses on the deformation part
            matching_deform_losses = self.deformation_network.get_matching_losses(mapping_s = mapping_s, mapping_t = mapping_t, reveresed_clusters = reveresed_clusters, v_batch = v_batch,\
            original_target = pos_normals_t[:,:3], original_source = pos_normals_s[:,:3], Pis = Pis, weights = weights_deformation_matching_loss)
            rigidity_deform_losses = self.deformation_network.get_rigidity_loss(weights = weights_deformation_rigidity_loss)

            rig_deform_loss = weights_deformation_rigidity_loss[1][0]*(sum(rigidity_deform_losses)/len(rigidity_deform_losses))
            match_deform_loss = weights_deformation_matching_loss[1][0]*(sum(matching_deform_losses)/len(matching_deform_losses))

            total_deformation_loss = rig_deform_loss + match_deform_loss
        if association_loss and deformation_loss:
            return total_deformation_loss, rig_deform_loss, match_deform_loss, matching_deform_losses, rigidity_deform_losses,\
            total_association_loss, geo_assos_loss, match_assos_loss, cycle_assos_loss, self_reconstruction_assos_loss, geodesic_assos_losses, cycle_assos_losses, reconstruction_assos_losses, matching_assos_losses
        if association_loss and (not deformation_loss):
            return total_association_loss, geo_assos_loss, match_assos_loss, cycle_assos_loss, self_reconstruction_assos_loss, geodesic_assos_losses, cycle_assos_losses, reconstruction_assos_losses, matching_assos_losses
        if deformation_loss and (not association_loss):
            return total_deformation_loss, rig_deform_loss, match_deform_loss, matching_deform_losses, rigidity_deform_losses

