from ast import Str
from operator import ge
from typing import Dict, List, Optional
from pydantic import BaseModel, validator
import yaml
from pathlib import Path
import mtpad

ROOT = Path(mtpad.__file__).resolve().parent.parent

# genral config params

class GeneralConfig(BaseModel):

    model_name: str
    train_dataset_name: str
    inference_dataset_name: str
    eval_dataset_name: str
    allow_hyperparameter_load: bool
    train: bool
    infer: bool
    eval: bool

# dataset paths params

class PathsConfig(BaseModel):

    scape_remeshed_aligned_datasets: str
    scape_remeshed_aligned_distance_matrices: str
    extended_faust_datasets: str
    extended_faust_distance_matrices: str
    extended_faust_path_to_test_pairs: Optional[str]


# model hyperparameters

class BidirectionalSmoothedMtpadVModelConfig(BaseModel):

    input_dim: Optional[int]
    association_hidden_dims: Optional[List[List[int]]]
    deformation_hidden_dims: Optional[List[List[int]]]
    aggregation_hidden_dims: Optional[List[List[int]]]
    bias: bool

# training hyperparameters

class BidirectionalSmoothedMtpadVTrainingConfig(BaseModel):

    lr: float
    epochs: int
    clip: float
    experiment_id: int
    patch_configuration: str
    adversarial_setup: bool
    association_iterations: int
    deformation_iterations: int
    geodesic_loss_weight: List[List[float]]
    spatial_cycle_loss: bool
    cycle_loss_weight: List[List[float]]
    spatial_self_reconstruction_loss: bool
    self_reconstruction_loss_weight: List[List[float]]
    association_matching_loss_weight: List[List[float]]
    deformation_matching_loss_weight: List[List[float]]
    rigidity_loss_weight: List[List[float]]
    mode: str
    sigmas: List[float]

# experiment hyperparams

class BidirectionalSmoothedMtpadVExperimentConfig(BaseModel):
    log: bool
    verbose: bool
    eval_every: int
    eval_train: bool
    eval_val: bool
    eval_test: bool








# creating the pydantic object from the yaml config file
def create_config():
    config_path = Path(ROOT, "config.yml")
    with open(config_path, "r") as f:
        config_dict = yaml.load(f, Loader=yaml.Loader)
    general_config = GeneralConfig(**config_dict["general_params"])
    paths_config = PathsConfig(**config_dict["path_params"])
    if general_config.model_name in ['smoothed_bidirectional_mtpad_v_local_noac']:
        model_config = BidirectionalSmoothedMtpadVModelConfig(**config_dict["model_params"]["bidirectional_smoothed_mtpad_v_model_params"])
        training_config = BidirectionalSmoothedMtpadVTrainingConfig(**config_dict["training_params"]["bidirectional_smoothed_mtpad_v_training_params"])
        experiment_config = BidirectionalSmoothedMtpadVExperimentConfig(**config_dict["experiment_params"]["bidirectional_smoothed_mtpad_v_experiment_params"])

    return general_config, paths_config, model_config, training_config, experiment_config

general_config, paths_config, model_config, training_config, experiment_config = create_config()
