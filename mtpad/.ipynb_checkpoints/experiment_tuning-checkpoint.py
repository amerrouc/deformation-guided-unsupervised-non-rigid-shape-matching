from turtle import mode
import torch
import numpy as np
from tqdm import tqdm

from torch.utils.tensorboard import SummaryWriter
from .tools.metrics import *
from torch_geometric.transforms import *
from .data import *
#from  .data import *
import os
import pickle
from torch_geometric.transforms import *
import copy
import torch.optim as optim
import time
import lzma

def fit(checkpoint ,train_loader, val_loader, test_loader, epochs, device, writer=None, clip=2.0, model_name = 'feastnet', dataset_name = 'faust', description = "", paths_config = {}, training_config = None,\
    logging={'log': True, 'verbose': True, 'eval_every': 10}, train_logs_path = None, selected_couples = None):
    
    # load checkpoint (everything is idempotent)
    if training_config.adversarial_setup:
        net, optimizer_association, scheduler_association, optimizer_deformation, scheduler_deformation, writer_path = checkpoint.model, checkpoint.optimizer_association,\
        checkpoint.scheduler_association, checkpoint.optimizer_deformation, checkpoint.scheduler_deformation, checkpoint.writer_path
    else:
        net, optimizer, scheduler, writer_path = checkpoint.model, checkpoint.optimizer, checkpoint.scheduler, checkpoint.writer_path
    # idempotent writer
    if writer is None:
        writer = SummaryWriter(writer_path)
    # minimas
    min_loss = float('inf')
    # num samples in train loader
    num_train_examples = len(train_loader)
    def train_epoch(epoch):
        def detach_list_of_tensor(list_of_tensors):
            for i in range(len(list_of_tensors)):
                for j in range(len(list_of_tensors[i])):
                    list_of_tensors[i][j] = list_of_tensors[i][j].detach()
            return list_of_tensors
        if logging['verbose']:
            iterator = tqdm(train_loader, desc=f'Epoch {epoch}/{epochs}', dynamic_ncols=True,  position=0, leave=True)
        else:
            iterator = train_loader
        epoch_loss = 0.
        
        net.train()
        for (i, batch) in enumerate(iterator):
            if model_name in ['smoothed_bidirectional_mtpad_v_local_noac']:
                targets_s, pos_normals_s, clusters_s, adjs_s, list_adjs_s, mapping_s, cluster_sizes_s, distance_matrices_s, point_cluster_weights_s, id_s, face_s,\
                targets_t, pos_normals_t, clusters_t, adjs_t, list_adjs_t, mapping_t, cluster_sizes_t, distance_matrices_t, point_cluster_weights_t, id_t, face_t, batch = prepare_batch(batch, model_name, dataset_name, device)
                
                optimizer.zero_grad()

                Pis, shapes = net(pos_normals_s = pos_normals_s, clusters_s = clusters_s, adjs_s = adjs_s, list_adjs_s = list_adjs_s, mapping_s = mapping_s, cluster_sizes_s = cluster_sizes_s, distance_matrices_s = distance_matrices_s,\
                    pos_normals_t = pos_normals_t, clusters_t = clusters_t, adjs_t = adjs_t, list_adjs_t = list_adjs_t, mapping_t = mapping_t, cluster_sizes_t = cluster_sizes_t, distance_matrices_t = distance_matrices_t,\
                    v_batch = batch, point_cluster_weights_s = point_cluster_weights_s, point_cluster_weights_t = point_cluster_weights_t, mode = training_config.mode, sigmas = training_config.sigmas)
                total_deformation_loss, rig_deform_loss, match_deform_loss, matching_deform_losses, rigidity_deform_losses,\
                total_association_loss, geo_assos_loss, match_assos_loss, cycle_assos_loss, self_reconstruction_assos_loss, geodesic_assos_losses, cycle_assos_losses, reconstruction_assos_losses, matching_assos_losses =\
                net.get_loss(shapes = shapes, Pis = Pis, pos_normals_s = pos_normals_s, pos_normals_t = pos_normals_t, clusters_s = clusters_s, clusters_t = clusters_t, cluster_sizes_s = cluster_sizes_s,\
                    distance_matrices_s = distance_matrices_s, distance_matrices_t = distance_matrices_t, mapping_s = mapping_s, mapping_t = mapping_t, v_batch = batch,\
                    weights_geodesic_distortion_loss = training_config.geodesic_loss_weight, weights_cycle_loss = training_config.cycle_loss_weight, weights_self_reconstruction_loss = training_config.self_reconstruction_loss_weight,\
                    weights_association_matching_loss = training_config.association_matching_loss_weight,  weights_deformation_rigidity_loss = training_config.rigidity_loss_weight,\
                    weights_deformation_matching_loss = training_config.deformation_matching_loss_weight, spatial_cycle_loss = training_config.spatial_cycle_loss, spatial_self_reconstruction_loss = training_config.spatial_self_reconstruction_loss,\
                    association_loss = True, deformation_loss = True)

                total_loss = total_deformation_loss + total_association_loss
                total_loss.backward()
                total_norm = torch.nn.utils.clip_grad_norm_(net.parameters(),max_norm=clip)
                optimizer.step()


                    

                if logging['log']:
                    # logging for asociation network

                    # total loss
                    writer.add_scalar('Total association loss', total_association_loss.item(), (epoch*num_train_examples)+i)

                    # geodesic loss
                    dict_asos_geo_loss = {"Level "+str(i+1) : v.item() for i,v in enumerate(geodesic_assos_losses)}
                    dict_asos_geo_loss["Total"] =  geo_assos_loss.item()
                    writer.add_scalars('Iteration Association Geodesic Loss', dict_asos_geo_loss, (epoch*num_train_examples)+i)

                    # matching loss
                    dict_asos_match_loss = {"Level "+str(i+1) : v.item() for i,v in enumerate(matching_assos_losses)}
                    dict_asos_match_loss["Total"] =  match_assos_loss.item()
                    writer.add_scalars('Iteration Association Matching Loss', dict_asos_match_loss, (epoch*num_train_examples)+i)

                    # map cycle loss
                    dict_asos_cycle_loss = {"Level "+str(i+1) : v.item() for i,v in enumerate(cycle_assos_losses)}
                    dict_asos_cycle_loss["Total"] =  cycle_assos_loss.item()
                    writer.add_scalars('Iteration Association Cycle Loss', dict_asos_cycle_loss, (epoch*num_train_examples)+i)

                    # map rec loss
                    dict_asos_reconstruction_loss = {"Level "+str(i+1) : v.item() for i,v in enumerate(reconstruction_assos_losses)}
                    dict_asos_reconstruction_loss["Total"] =  self_reconstruction_assos_loss.item()
                    writer.add_scalars('Iteration Association Reconstruction Loss', dict_asos_reconstruction_loss, (epoch*num_train_examples)+i)

                    # logging for deformation network

                    # total loss
                    writer.add_scalar('Total deformation loss', total_deformation_loss.item(), (epoch*num_train_examples)+i)

                    # rigidity loss
                    dict_deform_rig_loss = {"Level "+str(i+1) : v.item() for i,v in enumerate(rigidity_deform_losses)}
                    dict_deform_rig_loss["Total"] =  rig_deform_loss.item()
                    writer.add_scalars('Iteration Deformation Rgidity Loss', dict_deform_rig_loss, (epoch*num_train_examples)+i)

                    # matchig loss
                    dict_deform_match_loss = {"Level "+str(i+1) : v.item() for i,v in enumerate(matching_deform_losses)}
                    dict_deform_match_loss["Total"] =  match_deform_loss.item()
                    writer.add_scalars('Iteration Deformation Matching Loss', dict_deform_match_loss, (epoch*num_train_examples)+i)

            epoch_loss += total_loss.item()
            if logging['verbose']:
                iterator.set_postfix(loss=f'{total_loss.item():.4e}')
            if logging['log']:
                writer.add_scalar('Total_norm_of_parameters_gradients', total_norm, (epoch*num_train_examples)+i)
        epoch_loss /= len(train_loader)
        return epoch_loss
    
    def evaluate_epoch(loader, log_deformations=False, skip=False, epoch_num=0):
        net.eval()
        # association
        mean_asoss_loss = 0.
        mean_assos_geo_loss = 0.
        mean_assos_matching_loss = 0.
        mean_assos_cycle_loss = 0.
        mean_assos_rec_loss = 0.
        # deofrmation 
        mean_deform_loss = 0.0 
        mean_deform_rigidity_loss = 0.
        mean_deform_matching_loss = 0.
        
        if skip:
            return 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0
        else:
            with torch.no_grad():
                for i,batch in enumerate(loader):
                    if model_name in ['smoothed_bidirectional_mtpad_v_local_noac']:
                        targets_s, pos_normals_s, clusters_s, adjs_s, list_adjs_s, mapping_s, cluster_sizes_s, distance_matrices_s, point_cluster_weights_s, id_s, face_s,\
                        targets_t, pos_normals_t, clusters_t, adjs_t, list_adjs_t, mapping_t, cluster_sizes_t, distance_matrices_t, point_cluster_weights_t, id_t, face_t, batch = prepare_batch(batch, model_name, dataset_name, device)
                        
                        

                        
                    
                        Pis, shapes = net(pos_normals_s = pos_normals_s, clusters_s = clusters_s, adjs_s = adjs_s, list_adjs_s = list_adjs_s, mapping_s = mapping_s, cluster_sizes_s = cluster_sizes_s, distance_matrices_s = distance_matrices_s,\
                        pos_normals_t = pos_normals_t, clusters_t = clusters_t, adjs_t = adjs_t, list_adjs_t = list_adjs_t, mapping_t = mapping_t, cluster_sizes_t = cluster_sizes_t, distance_matrices_t = distance_matrices_t,\
                        v_batch = batch, point_cluster_weights_s = point_cluster_weights_s, point_cluster_weights_t = point_cluster_weights_t, mode = training_config.mode, sigmas = training_config.sigmas)
                        
                        total_deformation_loss, rig_deform_loss, match_deform_loss, matching_deform_losses, rigidity_deform_losses,\
                        total_association_loss, geo_assos_loss, match_assos_loss, cycle_assos_loss, self_reconstruction_assos_loss, geodesic_assos_losses, cycle_assos_losses, reconstruction_assos_losses, matching_assos_losses =\
                        net.get_loss(shapes = shapes, Pis = Pis, pos_normals_s = pos_normals_s, pos_normals_t = pos_normals_t, clusters_s = clusters_s, clusters_t = clusters_t, cluster_sizes_s = cluster_sizes_s,\
                            distance_matrices_s = distance_matrices_s, distance_matrices_t = distance_matrices_t, mapping_s = mapping_s, mapping_t = mapping_t, v_batch = batch,\
                            weights_geodesic_distortion_loss = training_config.geodesic_loss_weight, weights_cycle_loss = training_config.cycle_loss_weight, weights_self_reconstruction_loss = training_config.self_reconstruction_loss_weight,\
                            weights_association_matching_loss = training_config.association_matching_loss_weight,  weights_deformation_rigidity_loss = training_config.rigidity_loss_weight,\
                            weights_deformation_matching_loss = training_config.deformation_matching_loss_weight, spatial_cycle_loss = training_config.spatial_cycle_loss, spatial_self_reconstruction_loss = training_config.spatial_self_reconstruction_loss,\
                            association_loss = True, deformation_loss = True)
                        
                    
                        total_loss = total_deformation_loss + total_association_loss


                        mean_asoss_loss += total_association_loss.item()
                        mean_assos_geo_loss += geo_assos_loss.item()
                        mean_assos_matching_loss += match_assos_loss.item()
                        mean_assos_cycle_loss += cycle_assos_loss.item()
                        mean_assos_rec_loss += self_reconstruction_assos_loss.item()
                    

                        mean_deform_loss += total_deformation_loss.item()
                        mean_deform_rigidity_loss += rig_deform_loss.item()
                        mean_deform_matching_loss += match_deform_loss.item()

                        # log deformations
                        if log_deformations:
                            if selected_couples is None:
                                log_pair = True
                            else:
                                log_pair =  ([id_s.item(), id_t.item()]  in selected_couples) or ([id_t.item(), id_s.item()]  in selected_couples)
                            if log_pair:
                                path = osp.join(train_logs_path, 'evals_epoch_{0:05d}')
                                path = path.format(epoch_num)
                                if not os.path.exists(path):
                                    os.makedirs(path)
                                dict_of_logs = {}
                                dict_of_logs["source_id"] = id_s.item()
                                dict_of_logs["target_id"] = id_t.item()
                                for i in range(len(net.deformation_network.offset_finders)-1):
                                    dict_of_logs["deformation_rotaton_matrix_s_t_"+str(i)] = net.deformation_network.offset_finders[i].rotations_s_t.cpu().numpy()
                                    
                                    dict_of_logs["deformation_rotaton_matrix_t_s_"+str(i)] = net.deformation_network.offset_finders[i].rotations_t_s.cpu().numpy()
                                    dict_of_logs["correspondences_matrix_s_s_"+str(i)] = net.association_network.Pis[i][0].cpu().numpy()
                                    dict_of_logs["correspondences_matrix_s_t_"+str(i)] = net.association_network.Pis[i][1].cpu().numpy()
                                    dict_of_logs["correspondences_matrix_t_t_"+str(i)] = net.association_network.Pis[i][2].cpu().numpy()
                                    dict_of_logs["correspondences_matrix_t_s_"+str(i)] = net.association_network.Pis[i][3].cpu().numpy()
                                i = i+1
                                dict_of_logs["correspondences_matrix_s_s_"+str(i)] = net.association_network.Pis[i][0].cpu().numpy()
                                dict_of_logs["correspondences_matrix_s_t_"+str(i)] = net.association_network.Pis[i][1].cpu().numpy()
                                dict_of_logs["correspondences_matrix_t_t_"+str(i)] = net.association_network.Pis[i][2].cpu().numpy()
                                dict_of_logs["correspondences_matrix_t_s_"+str(i)] = net.association_network.Pis[i][3].cpu().numpy()
                                file_name = osp.join(path, 'shapes_{0:05d}_{1:05d}').format(id_s.item(), id_t.item())
                                dict_of_logs["shapes"] = [[pos_normals_s[:,:3].cpu().numpy()]+[a.cpu().numpy() for a in net.deformation_network.shapes[0]]+[pos_normals_t[:,:3].cpu().numpy()],\
                                [pos_normals_t[:,:3].cpu().numpy()]+[a.cpu().numpy() for a in net.deformation_network.shapes[1]]+[pos_normals_s[:,:3].cpu().numpy()]]
                                with open(file_name, 'wb') as f:
                                    pickle.dump(dict_of_logs, f)
                    
                return mean_asoss_loss / len(loader), mean_assos_geo_loss / len(loader), mean_assos_matching_loss / len(loader), mean_assos_cycle_loss / len(loader), mean_assos_rec_loss / len(loader),\
                mean_deform_loss / len(loader), mean_deform_rigidity_loss / len(loader), mean_deform_matching_loss / len(loader)
    # begin where we stopped last                                                                   
    begin_epoch = checkpoint.epoch
    for epoch in range(begin_epoch, epochs+1):
        epoch_loss = train_epoch(epoch)
        if training_config.adversarial_setup:
            scheduler_association.step()
            scheduler_deformation.step()
        else:
            scheduler.step()
        if epoch%logging['eval_every'] == 0:
            assos_loss_train, assos_geo_loss_train, assos_matching_loss_train, assos_cycle_loss_train, assos_rec_loss_train,\
            deform_loss_train, deform_rig_loss_train, deform_match_loss_train = evaluate_epoch(loader = train_loader, log_deformations=False, skip= not logging['eval_train'], epoch_num=epoch)

            assos_loss_val, assos_geo_loss_val, assos_matching_loss_val, assos_cycle_loss_val, assos_rec_loss_val,\
            deform_loss_val, deform_rig_loss_val, deform_match_loss_val = evaluate_epoch(loader = val_loader, log_deformations=False, skip=not logging['eval_val'], epoch_num=epoch)

            assos_loss_test, assos_geo_loss_test, assos_matching_loss_test, assos_cycle_loss_test, assos_rec_loss_test,\
            deform_loss_test, deform_rig_loss_test, deform_match_loss_test = evaluate_epoch(loader = test_loader, log_deformations=True, skip=not logging['eval_test'], epoch_num=epoch)

            if logging['verbose']:
                print(description)
                print(f"Epoch {epoch}/{epochs}, Train Loss: {assos_loss_train+deform_loss_train:.4f}, Test Loss: {assos_loss_test+deform_loss_test:.4f}, Val Loss: {assos_loss_val+deform_loss_val:.4f}")
            
            checkpoint.epoch += 1

            if logging['log']:
                writer.add_scalars("Epoch Association Loss", {"Train": assos_loss_train, "Val" : assos_loss_val,"Test" : assos_loss_test}, epoch)
                writer.add_scalars("Epoch Deformation Loss", {"Train": deform_loss_train, "Val" : deform_loss_val,"Test" : deform_loss_test}, epoch)
                if logging['log']:
                    writer.add_scalars("Epoch Loss", {"Val":assos_loss_val+deform_loss_val, "Train":epoch_loss}, epoch)

                writer.add_scalars("Epoch Association Geodesic Loss", {"Train": assos_geo_loss_train, "Val" : assos_geo_loss_val,"Test" : assos_geo_loss_test}, epoch)
                writer.add_scalars("Epoch Association Cycle Loss", {"Train": assos_cycle_loss_train, "Val" : assos_cycle_loss_val,"Test" : assos_cycle_loss_test}, epoch)
                writer.add_scalars("Epoch Association Self Reconstructon Loss", {"Train": assos_rec_loss_train, "Val" : assos_rec_loss_val,"Test" : assos_rec_loss_test}, epoch)
                writer.add_scalars("Epoch Association Matching Loss", {"Train": assos_matching_loss_train, "Val" : assos_matching_loss_val,"Test" : assos_matching_loss_test}, epoch)


                writer.add_scalars("Epoch Deformation Rigidity Loss", {"Train": deform_rig_loss_train, "Val" : deform_rig_loss_val,"Test" : deform_rig_loss_test}, epoch)
                writer.add_scalars("Epoch Deformation Matching Loss", {"Train": deform_match_loss_train, "Val" : deform_match_loss_val,"Test" : deform_match_loss_test}, epoch)
            writer.flush()

            loss_val = assos_loss_val+deform_loss_val
            if loss_val < min_loss:
                min_loss = loss_val
                if logging['verbose']:
                    print("New Best Loss : ", loss_val)
                checkpoint.save('_best_loss')
        else:
            assos_loss_val, assos_geo_loss_val, assos_matching_loss_val, assos_cycle_loss_val, assos_rec_loss_val,\
            deform_loss_val, deform_rig_loss_val, deform_match_loss_val = evaluate_epoch(loader = val_loader, log_deformations=False, skip=False, epoch_num=epoch)
            if logging['log']:
                writer.add_scalars("Epoch Loss", {"Val":assos_loss_val+deform_loss_val, "Train":epoch_loss}, epoch)
            loss_val = assos_loss_val+deform_loss_val
            if loss_val < min_loss:
                min_loss = loss_val
                if logging['verbose']:
                    print("New Best Loss : ", loss_val)
                checkpoint.save('_best_loss')
            checkpoint.epoch += 1
        checkpoint.save()
    if logging['verbose']:
        print(description)
        print("\nFinished.")
    writer.close()



def eval(checkpoint ,eval_loader, device, model_name = 'feastnet', dataset_name = 'faust', eval_logs_path=None,\
    paths_config = {}, training_config=None, selected_couples=None):
    
    def evaluate_pair(pair_args, path_to_save, net):
        net.eval()
        with torch.no_grad():
            if model_name in ['smoothed_bidirectional_mtpad_v_local_noac']:
                targets_s, pos_normals_s, clusters_s, adjs_s, list_adjs_s, mapping_s, cluster_sizes_s, distance_matrices_s, point_cluster_weights_s, id_s, face_s,\
                targets_t, pos_normals_t, clusters_t, adjs_t, list_adjs_t, mapping_t, cluster_sizes_t, distance_matrices_t, point_cluster_weights_t, id_t, face_t, batch = pair_args.values()
                t0 = time.time()
                Pis = net.association_network(pos_normals_s = pos_normals_s, clusters_s = clusters_s, adjs_s = adjs_s, cluster_sizes_s = cluster_sizes_s, pos_normals_t = pos_normals_t, clusters_t = clusters_t, adjs_t = adjs_t,\
                v_batch = batch, sigmas = training_config.sigmas, mode = training_config.mode)
                t1 = time.time()
                t2 = time.time()

                Pi_s_s, Pi_s_t, Pi_t_t, Pi_t_s = Pis[-1]
                #pred_s_t = get_closest_point(transformed_source.cpu().numpy(), target_shape.cpu().numpy())
                pred_s_t = Pi_s_t.argmax(dim=-1).cpu().numpy()
                #pred_t_s = get_closest_point(transformed_target.cpu().numpy(), source_shape.cpu().numpy())
                pred_t_s = Pi_t_s.argmax(dim=-1).cpu().numpy()
                t3 = time.time()

                print("corres : ", t1-t0)
                print("get loss : ", t2-t1)
                print("get geoerror : ", t3-t2)

        return pred_s_t, pred_t_s

    
    for (i, pair) in enumerate(eval_loader):
        if model_name in ['smoothed_bidirectional_mtpad_v_local_noac']:
            targets_s, pos_normals_s, clusters_s, adjs_s, list_adjs_s, mapping_s, cluster_sizes_s, distance_matrices_s, point_cluster_weights_s, id_s, face_s,\
            targets_t, pos_normals_t, clusters_t, adjs_t, list_adjs_t, mapping_t, cluster_sizes_t, distance_matrices_t, point_cluster_weights_t, id_t, face_t, batch = prepare_batch(batch = pair, model_name = model_name, dataset_name = dataset_name, device = device)
            pair_args = {'targets_s' : targets_s, 'pos_normals_s' : pos_normals_s, 'clusters_s' : clusters_s, 'adjs_s' : adjs_s, 'list_adjs_s' : list_adjs_s, 'mapping_s' : mapping_s, 'cluster_sizes_s' : cluster_sizes_s,\
                'distance_matrices_s' : distance_matrices_s, 'point_cluster_weights_s' : point_cluster_weights_s, 'id_s' : id_s, 'face_s' : face_s, 'targets_t' : targets_t, 'pos_normals_t' : pos_normals_t, 'clusters_t' : clusters_t,\
                'adjs_t' : adjs_t, 'list_adjs_t' : list_adjs_t, 'mapping_t' : mapping_t, 'cluster_sizes_t' : cluster_sizes_t, 'distance_matrices_t' : distance_matrices_t, 'point_cluster_weights_t' : point_cluster_weights_t,\
                'id_t' : id_t, 'face_t' : face_t, 'batch' : batch}
        if selected_couples is None:
            eval_pair = True
        else:
            eval_pair =  ([pair_args['id_s'].item(), pair_args['id_t'].item()]  in selected_couples) or ([pair_args['id_t'].item(), pair_args['id_s'].item()]  in selected_couples)
        if training_config.adversarial_setup:
            net = checkpoint.model
        else:
            net = checkpoint.model
        if eval_pair:
            # this is where we stopped last
            path = osp.join(eval_logs_path)
            pred_s_t, pred_t_s = evaluate_pair(pair_args = pair_args, path_to_save = path, net = net)
            print(f"Pair {i}/{len(eval_loader)}, {pair_args['id_s'].item(), pair_args['id_t'].item()}")

            folder_to_save = osp.join(path, 'predictions')
            if not os.path.exists(folder_to_save):
                os.makedirs(folder_to_save)
            if dataset_name in ['extended_faust']:

                path_to_id_name = osp.join(paths_config[dataset_name+"_distance_matrices"], 'id_name_extended_faust.pkl')
                with open(path_to_id_name, "rb") as fp:   # Unpickling
                    id_name_extended_faust = pickle.load(fp)
                shape_1 = id_name_extended_faust[pair_args['id_s'].item()]
                shape_2 = id_name_extended_faust[pair_args['id_t'].item()]
                with open(osp.join(folder_to_save, shape_1+"%"+shape_2+'.npy'), 'wb') as f:
                    np.save(f, pred_s_t)
                with open(osp.join(folder_to_save, shape_2+"%"+shape_1+'.npy'), 'wb') as f:
                    np.save(f, pred_t_s)
            if dataset_name == 'scape_remeshed_aligned':
                
                shape_1 = 'mesh{0:03d}'.format(pair_args['id_s'].item())
                shape_2 = 'mesh{0:03d}'.format(pair_args['id_t'].item())
                with open(osp.join(folder_to_save, shape_1+"%"+shape_2+'.npy'), 'wb') as f:
                    np.save(f, pred_s_t)
                with open(osp.join(folder_to_save, shape_2+"%"+shape_1+'.npy'), 'wb') as f:
                    np.save(f, pred_t_s)
            

def infer(checkpoint ,inference_loader, epochs, device, clip=2.0, model_name = 'feastnet', dataset_name = 'faust', inference_logs_path=None, lr=0.001,\
    freeze_association=False, freeze_deformation=False, paths_config = {}, models = None, model_config = None, training_config=None, selected_couples=None, log_deformations=False):
    def get_new_net_optimizer(checkpoint, model_name, models, model_config, training_config, device):
        net_pair = models[model_name](**model_config.dict()).double().to(device)
        net_pair.load_state_dict(copy.deepcopy(checkpoint.model.state_dict()))
        if training_config.adversarial_setup:
            optimizer_association_pair = optim.Adam(net_pair.association_network.parameters(), training_config.lr)
            optimizer_association_pair.load_state_dict(copy.deepcopy(checkpoint.optimizer_association.state_dict()))
            optimizer_deformation_pair = optim.Adam(net_pair.deformation_network.parameters(), training_config.lr)
            optimizer_deformation_pair.load_state_dict(copy.deepcopy(checkpoint.optimizer_deformation.state_dict()))
            scheduler_association_pair = torch.optim.lr_scheduler.StepLR(optimizer = optimizer_association_pair, step_size = 5, gamma=1.0, last_epoch=- 1, verbose=False)
            scheduler_association_pair.load_state_dict(copy.deepcopy(checkpoint.scheduler_association.state_dict()))
            scheduler_deformation_pair = torch.optim.lr_scheduler.StepLR(optimizer = optimizer_deformation_pair, step_size = 5, gamma=1.0, last_epoch=- 1, verbose=False)
            scheduler_deformation_pair.load_state_dict(copy.deepcopy(checkpoint.scheduler_deformation.state_dict()))
            optimizer = (optimizer_association_pair, optimizer_deformation_pair)
            scheduler = (scheduler_association_pair, scheduler_deformation_pair)
            return net_pair, optimizer[0], scheduler[0], optimizer[1], scheduler[1]
        else:
            optimizer_pair = optim.Adam(net_pair.parameters(), training_config.lr)
            optimizer_pair.load_state_dict(copy.deepcopy(checkpoint.optimizer.state_dict()))
            #scheduler_pair = torch.optim.lr_scheduler.StepLR(optimizer = optimizer_pair, step_size = 5, gamma=1.0, last_epoch=- 1, verbose=False)
            scheduler_pair = torch.optim.lr_scheduler.MultiStepLR(optimizer = optimizer_pair, milestones=[1, 10, 40], gamma=1/2)
            scheduler_pair.load_state_dict(copy.deepcopy(checkpoint.scheduler.state_dict()))
            return net_pair, optimizer_pair, scheduler_pair
    def train_pair(pair_args, net_pair):
        if model_name in ['smoothed_bidirectional_mtpad_v_local_noac']:
            targets_s, pos_normals_s, clusters_s, adjs_s, list_adjs_s, mapping_s, cluster_sizes_s, distance_matrices_s, point_cluster_weights_s, id_s, face_s,\
            targets_t, pos_normals_t, clusters_t, adjs_t, list_adjs_t, mapping_t, cluster_sizes_t, distance_matrices_t, point_cluster_weights_t, id_t, face_t, batch = pair_args.values()
            shapes = None
            Pis = None
            
            t0 = time.time()
            optimizer_pair.zero_grad()
            
            
            Pis, shapes = net_pair(pos_normals_s = pos_normals_s, clusters_s = clusters_s, adjs_s = adjs_s, list_adjs_s = list_adjs_s, mapping_s = mapping_s, cluster_sizes_s = cluster_sizes_s, distance_matrices_s = distance_matrices_s,\
            pos_normals_t = pos_normals_t, clusters_t = clusters_t, adjs_t = adjs_t, list_adjs_t = list_adjs_t, mapping_t = mapping_t, cluster_sizes_t = cluster_sizes_t, distance_matrices_t = distance_matrices_t,\
            v_batch = batch, point_cluster_weights_s = point_cluster_weights_s, point_cluster_weights_t = point_cluster_weights_t, mode = training_config.mode, sigmas = training_config.sigmas)
            total_deformation_loss, rig_deform_loss, match_deform_loss, matching_deform_losses, rigidity_deform_losses,\
            total_association_loss, geo_assos_loss, match_assos_loss, cycle_assos_loss, self_reconstruction_assos_loss, geodesic_assos_losses, cycle_assos_losses, reconstruction_assos_losses, matching_assos_losses =\
            net_pair.get_loss(shapes = shapes, Pis = Pis, pos_normals_s = pos_normals_s, pos_normals_t = pos_normals_t, clusters_s = clusters_s, clusters_t = clusters_t, cluster_sizes_s = cluster_sizes_s,\
                distance_matrices_s = distance_matrices_s, distance_matrices_t = distance_matrices_t, mapping_s = mapping_s, mapping_t = mapping_t, v_batch = batch,\
                weights_geodesic_distortion_loss = training_config.geodesic_loss_weight, weights_cycle_loss = training_config.cycle_loss_weight, weights_self_reconstruction_loss = training_config.self_reconstruction_loss_weight,\
                weights_association_matching_loss = training_config.association_matching_loss_weight,  weights_deformation_rigidity_loss = training_config.rigidity_loss_weight,\
                weights_deformation_matching_loss = training_config.deformation_matching_loss_weight, spatial_cycle_loss = training_config.spatial_cycle_loss, spatial_self_reconstruction_loss = training_config.spatial_self_reconstruction_loss,\
                association_loss = True, deformation_loss = True)
            

            total_loss = total_deformation_loss + total_association_loss
            t1 = time.time()
            #print("Forward : ", t1-t0)
            total_loss.backward()
            total_norm = torch.nn.utils.clip_grad_norm_(net_pair.parameters(),max_norm=clip)
            optimizer_pair.step()
            t2 = time.time()
            #print("Backward : ", t2-t1)
        return total_loss.item()
    def evaluate_pair(pair_args, path_to_save, net_pair, log_deformations=True):
        net_pair.eval()
        with torch.no_grad():
            if model_name in ['smoothed_bidirectional_mtpad_v_local_noac']:
                targets_s, pos_normals_s, clusters_s, adjs_s, list_adjs_s, mapping_s, cluster_sizes_s, distance_matrices_s, point_cluster_weights_s, id_s, face_s,\
                targets_t, pos_normals_t, clusters_t, adjs_t, list_adjs_t, mapping_t, cluster_sizes_t, distance_matrices_t, point_cluster_weights_t, id_t, face_t, batch = pair_args.values()
                
                
            
                
                Pis, shapes = net_pair(pos_normals_s = pos_normals_s, clusters_s = clusters_s, adjs_s = adjs_s, list_adjs_s = list_adjs_s, mapping_s = mapping_s, cluster_sizes_s = cluster_sizes_s, distance_matrices_s = distance_matrices_s,\
                pos_normals_t = pos_normals_t, clusters_t = clusters_t, adjs_t = adjs_t, list_adjs_t = list_adjs_t, mapping_t = mapping_t, cluster_sizes_t = cluster_sizes_t, distance_matrices_t = distance_matrices_t,\
                v_batch = batch, point_cluster_weights_s = point_cluster_weights_s, point_cluster_weights_t = point_cluster_weights_t, mode = training_config.mode, sigmas = training_config.sigmas)

                total_deformation_loss, rig_deform_loss, match_deform_loss, matching_deform_losses, rigidity_deform_losses,\
                total_association_loss, geo_assos_loss, match_assos_loss, cycle_assos_loss, self_reconstruction_assos_loss, geodesic_assos_losses, cycle_assos_losses, reconstruction_assos_losses, matching_assos_losses =\
                net_pair.get_loss(shapes = shapes, Pis = Pis, pos_normals_s = pos_normals_s, pos_normals_t = pos_normals_t, clusters_s = clusters_s, clusters_t = clusters_t, cluster_sizes_s = cluster_sizes_s,\
                    distance_matrices_s = distance_matrices_s, distance_matrices_t = distance_matrices_t, mapping_s = mapping_s, mapping_t = mapping_t, v_batch = batch,\
                    weights_geodesic_distortion_loss = training_config.geodesic_loss_weight, weights_cycle_loss = training_config.cycle_loss_weight, weights_self_reconstruction_loss = training_config.self_reconstruction_loss_weight,\
                    weights_association_matching_loss = training_config.association_matching_loss_weight,  weights_deformation_rigidity_loss = training_config.rigidity_loss_weight,\
                    weights_deformation_matching_loss = training_config.deformation_matching_loss_weight, spatial_cycle_loss = training_config.spatial_cycle_loss, spatial_self_reconstruction_loss = training_config.spatial_self_reconstruction_loss,\
                    association_loss = True, deformation_loss = True)
            
                total_loss = total_deformation_loss + total_association_loss

                Pi_s_s, Pi_s_t, Pi_t_t, Pi_t_s = Pis[-1]
                #pred_s_t = get_closest_point(transformed_source.cpu().nu4794/5995mpy(), target_shape.cpu().numpy())
                pred_s_t = Pi_s_t.argmax(dim=-1).cpu().numpy()
                #pred_t_s = get_closest_point(transformed_target.cpu().numpy(), source_shape.cpu().numpy())
                pred_t_s = Pi_t_s.argmax(dim=-1).cpu().numpy()
                folder_to_save = osp.join(path_to_save, 'predictions')
                if not os.path.exists(folder_to_save):
                    os.makedirs(folder_to_save)

                if dataset_name in ['extended_faust']:
                
                    path_to_id_name = osp.join(paths_config[dataset_name+"_distance_matrices"], 'id_name_extended_faust.pkl')
                    with open(path_to_id_name, "rb") as fp:   # Unpickling
                        id_name_extended_faust = pickle.load(fp)
                    shape_1 = id_name_extended_faust[pair_args['id_s'].item()]
                    shape_2 = id_name_extended_faust[pair_args['id_t'].item()]
                    with open(osp.join(folder_to_save, shape_1+"%"+shape_2+'.npy'), 'wb') as f:
                        np.save(f, pred_s_t)
                    with open(osp.join(folder_to_save, shape_2+"%"+shape_1+'.npy'), 'wb') as f:
                        np.save(f, pred_t_s)

                if dataset_name == 'scape_remeshed_aligned':
                    
                    shape_1 = 'mesh{0:03d}'.format(pair_args['id_s'].item())
                    shape_2 = 'mesh{0:03d}'.format(pair_args['id_t'].item())
                    with open(osp.join(folder_to_save, shape_1+"%"+shape_2+'.npy'), 'wb') as f:
                        np.save(f, pred_s_t)
                    with open(osp.join(folder_to_save, shape_2+"%"+shape_1+'.npy'), 'wb') as f:
                        np.save(f, pred_t_s)
                
                        
                        
                if log_deformations:
                    if not os.path.exists(path_to_save):
                        os.makedirs(path)
                    dict_of_logs = {}
                    dict_of_logs["source_id"] = id_s.item()
                    dict_of_logs["target_id"] = id_t.item()
                    for i in range(len(net_pair.deformation_network.offset_finders)-1):
                        dict_of_logs["deformation_rotaton_matrix_s_t_"+str(i)] = net_pair.deformation_network.offset_finders[i].rotations_s_t.cpu().numpy()
                        dict_of_logs["deformation_rotaton_matrix_t_s_"+str(i)] = net_pair.deformation_network.offset_finders[i].rotations_t_s.cpu().numpy()
                        dict_of_logs["correspondences_matrix_s_s_"+str(i)] = net_pair.association_network.Pis[i][0].cpu().numpy()
                        dict_of_logs["correspondences_matrix_s_t_"+str(i)] = net_pair.association_network.Pis[i][1].cpu().numpy()
                        dict_of_logs["correspondences_matrix_t_t_"+str(i)] = net_pair.association_network.Pis[i][2].cpu().numpy()
                        dict_of_logs["correspondences_matrix_t_s_"+str(i)] = net_pair.association_network.Pis[i][3].cpu().numpy()
                    i = i+1
                    dict_of_logs["correspondences_matrix_s_s_"+str(i)] = net_pair.association_network.Pis[i][0].cpu().numpy()
                    dict_of_logs["correspondences_matrix_s_t_"+str(i)] = net_pair.association_network.Pis[i][1].cpu().numpy()
                    dict_of_logs["correspondences_matrix_t_t_"+str(i)] = net_pair.association_network.Pis[i][2].cpu().numpy()
                    dict_of_logs["correspondences_matrix_t_s_"+str(i)] = net_pair.association_network.Pis[i][3].cpu().numpy()
                    file_name = osp.join(path, 'shapes_{0:05d}_{1:05d}').format(id_s.item(), id_t.item())
                    dict_of_logs["shapes"] = [[pos_normals_s[:,:3].cpu().numpy()]+[a.cpu().numpy() for a in net_pair.deformation_network.shapes[0]]+[pos_normals_t[:,:3].cpu().numpy()],\
                    [pos_normals_t[:,:3].cpu().numpy()]+[a.cpu().numpy() for a in net_pair.deformation_network.shapes[1]]+[pos_normals_s[:,:3].cpu().numpy()]]
                    with open(file_name, 'wb') as f:
                        pickle.dump(dict_of_logs, f)
        return total_loss.item()

    for (i, pair) in enumerate(inference_loader):
        if i > -1:
            min_loss = float('inf')
            if model_name in ['smoothed_bidirectional_mtpad_v_local_noac']:
                targets_s, pos_normals_s, clusters_s, adjs_s, list_adjs_s, mapping_s, cluster_sizes_s, distance_matrices_s, point_cluster_weights_s, id_s, face_s,\
                targets_t, pos_normals_t, clusters_t, adjs_t, list_adjs_t, mapping_t, cluster_sizes_t, distance_matrices_t, point_cluster_weights_t, id_t, face_t, batch = prepare_batch(batch = pair, model_name = model_name, dataset_name = dataset_name, device = device, infer=False)
                pair_args = {'targets_s' : targets_s, 'pos_normals_s' : pos_normals_s, 'clusters_s' : clusters_s, 'adjs_s' : adjs_s, 'list_adjs_s' : list_adjs_s, 'mapping_s' : mapping_s, 'cluster_sizes_s' : cluster_sizes_s,\
                    'distance_matrices_s' : distance_matrices_s, 'point_cluster_weights_s' : point_cluster_weights_s, 'id_s' : id_s, 'face_s' : face_s, 'targets_t' : targets_t, 'pos_normals_t' : pos_normals_t, 'clusters_t' : clusters_t,\
                    'adjs_t' : adjs_t, 'list_adjs_t' : list_adjs_t, 'mapping_t' : mapping_t, 'cluster_sizes_t' : cluster_sizes_t, 'distance_matrices_t' : distance_matrices_t, 'point_cluster_weights_t' : point_cluster_weights_t,\
                    'id_t' : id_t, 'face_t' : face_t, 'batch' : batch}
            if selected_couples is None:
                log_pair = True
            else:
                log_pair =  ([pair_args['id_s'].item(), pair_args['id_t'].item()]  in selected_couples) or ([pair_args['id_t'].item(), pair_args['id_s'].item()]  in selected_couples)
            if log_pair:
                # load copy of netwrosk, optimizers and scheduelers (a copy for each pair)
                # load checkpoint (everything is idempotent)
                if training_config.adversarial_setup:
                    net_pair, optimizer_association_pair, scheduler_association_pair, optimizer_deformation_pair, scheduler_deformation_pair = get_new_net_optimizer(checkpoint = checkpoint, model_name = model_name, models = models,\
                    model_config = model_config, training_config = training_config, device = device)
                else:
                    net_pair, optimizer_pair, scheduler_pair = get_new_net_optimizer(checkpoint = checkpoint, model_name = model_name, models = models,\
                    model_config = model_config, training_config = training_config, device = device)
                # freeze association network
                if freeze_association:
                    for param in net_pair.association_network.parameters():
                        param.requires_grad = False
                # freeze deformation network
                if freeze_deformation:
                    for param in net_pair.deformation_network.parameters():
                        param.requires_grad = False
                # predict for pair and save
                print("Best epoch (=) : ", checkpoint.epoch)
                # this is where we stopped last
                path = osp.join(inference_logs_path, 'before_fine_tuning')
                loss = evaluate_pair(pair_args = pair_args, path_to_save = path, net_pair = net_pair, log_deformations=log_deformations)
                
                
                print(f"Pair {i}/{len(inference_loader)}, Before FT Loss: {loss:.4f}")
                # launch some training
                iterator = tqdm(np.arange(epochs), desc=f'Pair {i}/{len(inference_loader)}', dynamic_ncols=True,  position=0, leave=True)
                for epoch in iterator:
                    epoch_loss = train_pair(pair_args = pair_args, net_pair=net_pair)
                    iterator.set_postfix(loss=f'{epoch_loss:.4e}')
                    if epoch_loss < min_loss:
                        min_loss = epoch_loss
                        net_best_loss = copy.deepcopy(net_pair.state_dict())
                    if training_config.adversarial_setup:
                        scheduler_association_pair.step()
                        scheduler_deformation_pair.step()
                    else:
                        scheduler_pair.step()
                path = osp.join(inference_logs_path, 'after_fine_tuning')
                best_net_pair = models[model_name](**model_config.dict()).double().to(device)
                best_net_pair.load_state_dict(net_best_loss)
                loss = evaluate_pair(pair_args = pair_args, path_to_save = path, net_pair=net_pair, log_deformations=log_deformations)

                print(f"Pair {i}/{len(inference_loader)}, After FT Loss: {loss:.4f}")
                print("\n")

def prepare_batch(batch, model_name, dataset_name, device='cuda', infer=False):
    def push_dict_to_device(dictionnary, device, double=False):
        for key in dictionnary:
            if torch.is_tensor(dictionnary[key]):
                if double:
                    dictionnary[key] = dictionnary[key].to(device).double()
                else:
                    dictionnary[key] = dictionnary[key].to(device)
            if isinstance(dictionnary[key], tuple):
                dictionnary[key] = (dictionnary[key][0].to(device), dictionnary[key][1].to(device))
        return dictionnary
    def prepare_one_of_a_pair(data, model_name, dataset_name, device='cuda'):
        if model_name in ['smoothed_bidirectional_mtpad_v_local_noac']:
            targets = data.labels.to(device)
            pos_normals = torch.cat([data.pos.double(), data.vertex_normals.double()], dim=-1).to(device)
            clusters = push_dict_to_device(data.clusters[0], device)
            adjs = push_dict_to_device(data.region_adjs[0], device)
            adjs[0] = data.edge_index.to(device)
            list_adjs = push_dict_to_device(data.region_list_adjs[0], device)
            mapping = data.mapping.to(device)
            cluster_sizes = push_dict_to_device(data.cluster_sizes[0], device)
            distance_matrices = push_dict_to_device(data.cluster_distances[0], device, double=True)
            point_cluster_weights = push_dict_to_device(data.point_to_cluster_weights[0], device)
            id = data.id.to(device)
            face = data.face.to(device)
            return targets, pos_normals, clusters, adjs, list_adjs, mapping, cluster_sizes, distance_matrices, point_cluster_weights, id, face
    if model_name in ['smoothed_bidirectional_mtpad_v_local_noac']:
        targets_s, pos_normals_s, clusters_s, adjs_s, list_adjs_s, mapping_s, cluster_sizes_s, distance_matrices_s, point_cluster_weights_s, id_s, face_s = prepare_one_of_a_pair(batch[0], model_name, dataset_name, device)
        targets_t, pos_normals_t, clusters_t, adjs_t, list_adjs_t, mapping_t, cluster_sizes_t, distance_matrices_t, point_cluster_weights_t, id_t, face_t = prepare_one_of_a_pair(batch[1], model_name, dataset_name, device)
        batch = torch.cat([torch.zeros(pos_normals_t.shape[0]), torch.ones(pos_normals_s.shape[0])], dim=-1).long().to(device)
        return targets_s, pos_normals_s, clusters_s, adjs_s, list_adjs_s, mapping_s, cluster_sizes_s, distance_matrices_s, point_cluster_weights_s, id_s, face_s,\
                targets_t, pos_normals_t, clusters_t, adjs_t, list_adjs_t, mapping_t, cluster_sizes_t, distance_matrices_t, point_cluster_weights_t, id_t, face_t, batch


