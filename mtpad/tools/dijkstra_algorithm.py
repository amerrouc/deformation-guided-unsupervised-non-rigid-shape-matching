import random
import numpy as np
import networkx as nx
import trimesh
from queue import PriorityQueue

def read_mesh_as_face_distance_graph(path):
    def get_distance_between_adj_faces(faces, vertices, face_adj, edges):
        centroids = vertices[faces].mean(axis=1)
        edge_pos = vertices[edges]
        def ClosestPointsOnLine(a, b, p):
            ap = p-a
            ab = b-a
            result = a + np.expand_dims(((ap*ab).sum(axis=-1))/((ab*ab).sum(axis=-1)), axis = -1) * ab
            return result
        v_prime = ClosestPointsOnLine(edge_pos[:,0], edge_pos[:,1], centroids[adj[:,0]])
        w_prime = ClosestPointsOnLine(edge_pos[:,0], edge_pos[:,1], centroids[adj[:,1]])
        
        v_v_prime = np.linalg.norm(v_prime - centroids[adj[:,0]], axis=-1)
        w_w_prime = np.linalg.norm(w_prime - centroids[adj[:,1]], axis=-1)
        v_prime_w_prime = np.linalg.norm(v_prime - w_prime, axis=-1)
        return np.sqrt((v_v_prime+w_w_prime)**2 + v_prime_w_prime**2)
    # load mesh in trimesh object
    mesh = trimesh.load_mesh(path, process=False)
    # build graph from mesh (eucliean distance on edges)
    vertices = mesh.vertices
    faces = mesh.faces
    adj, edges = trimesh.graph.face_adjacency(mesh=mesh, return_edges=True)
    centroids = vertices[faces].mean(axis=1)

    graphx = nx.Graph()
    graphx.add_nodes_from(np.arange(faces.shape[0]))
    graphx.add_edges_from(adj)
    # adding edge weights (l2 distance between points)
    keys = [tuple(i) for i in adj]
    values = list(np.linalg.norm(centroids[adj[:,0]] - centroids[adj[:,1]], axis=-1))
    geo_faces = get_distance_between_adj_faces(faces, vertices, adj, edges)
    values = list(geo_faces)
    nx.set_edge_attributes(graphx, values = dict(zip(keys, values)), name = 'weight')
    return graphx, mesh

def read_mesh_as_vertex_distance_graph(path):
    # load mesh in trimesh object
    mesh = trimesh.load_mesh(path, process=False)
    # build graph from mesh (eucliean distance on edges)
    vertices = mesh.vertices
    adj = np.transpose(np.nonzero(nx.to_numpy_array(mesh.vertex_adjacency_graph)))

    # build graph from mesh (eucliean distance on edges)
    graphx = nx.Graph()
    graphx.add_nodes_from(np.arange(vertices.shape[0]))
    graphx.add_edges_from(adj)
    # adding edge weights (l2 distance between points)
    keys = [tuple(i) for i in adj]
    values = list(np.sqrt(((vertices[adj[:,0]] - vertices[adj[:,1]]) ** 2).sum(axis = -1)))
    nx.set_edge_attributes(graphx, values = dict(zip(keys, values)), name = 'weight')
    return graphx, mesh


def dijkstra(graphx, start_vertex, old_distance = None):
    
    num_vertices = graphx.number_of_nodes()
    distance = np.full((num_vertices), np.inf)
    if old_distance is None:
        old_distance = np.full((num_vertices), np.inf)
    distance[start_vertex] = 0.0
    fixed = np.full((num_vertices),False)
    queue = PriorityQueue()
    queue.put((0, start_vertex))

    while not queue.empty():
        (dist, current_vertex) = queue.get()
        fixed[current_vertex] = True
        # we are starting new fronts in the same distance map (the power of monotonicity)
        if dist<old_distance[current_vertex]:
            for neighbour in graphx.neighbors(current_vertex):
                edge_weight = graphx.get_edge_data(current_vertex, neighbour)['weight']
                if not fixed[neighbour]:
                    old_cost = distance[neighbour]
                    new_cost = distance[current_vertex] + edge_weight
                    if new_cost < old_cost:
                        queue.put((new_cost, neighbour))
                        distance[neighbour] = new_cost
    return distance

def dijkstra_geodesic_clustering(graphx, num_regions = None, radius = None , fancy_init = False):
    # step 1 : choose random node
    num_vertices = graphx.number_of_nodes()
    current_node = random.choice(np.arange(num_vertices))
    # step 2 : build U_n
    U_n = dijkstra(graphx, current_node, old_distance = None)
    if fancy_init:
        current_node = np.argmax(U_n)
        U_n = dijkstra(graphx, current_node, old_distance = None)
    clusters = np.full((num_vertices), current_node)
    # the iteration
    if radius is not None:
        min_dist = np.inf
        while min_dist > radius:
            # step 3 : choose farthest away node
            current_node = np.argmax(U_n)
            min_dist = U_n[current_node]
            # step 4 : new map
            U_nn = dijkstra(graphx, current_node, old_distance = U_n)

            clusters[U_nn < U_n] = current_node
            U_n = np.minimum(U_n, U_nn)
    else:
        for i in range(num_regions-1):
            # step 3 : choose farthest away node
            current_node = np.argmax(U_n)
            # step 4 : new map
            U_nn = dijkstra(graphx, current_node, old_distance = U_n)

            clusters[U_nn < U_n] = current_node
            U_n = np.minimum(U_n, U_nn)
        
    # in clusters : indices are vertices and values are cluster centers
    # memebership matrix : clusters are numbered in the order of their cluster ids
    # give ids to regions (based on order of their center's id), also return the mapping to get the centers
    cluster_centers = np.unique(clusters)
    mapping = dict(zip(cluster_centers, np.arange(len(cluster_centers))))
    clusters = np.array([mapping[elt] for elt in clusters])
    
    # clusters [vertex_id (sorted):cluster_id]
    # mapping {cluster_id : centroid}
    return clusters, {v: k for k, v in mapping.items()}

def geodesic_clustering_medoids(graphx, num_regions):
    # step 1 : choose random node
    num_vertices = graphx.number_of_nodes()
    vertices = np.arange(num_vertices)
    # compute matrix of gedesic distances and replace start vertex (previous cluster center) 
    geodesic_distances = np.zeros((num_vertices, num_vertices))
    for i in range(num_vertices):
        geodesic_distances[i] = dijkstra(graphx, i, old_distance = None)

    medoids = np.random.choice(a=vertices, size=num_regions, replace=False)
    
    not_stable = True
    k = 0
    while not_stable:
        k+= 1
        not_stable = False
        # assign medoids
        geodesic_distances_restricted = geodesic_distances[:,medoids]
        clusters = medoids[np.argmin(geodesic_distances_restricted, axis=-1)]
        # reestimate medoids
        for i, medoid in enumerate(medoids):
            cluster_memebership = clusters == medoid
            new_medoid = vertices[cluster_memebership][np.argmin((geodesic_distances[cluster_memebership,:][:,cluster_memebership]).sum(axis = -1))]
            # if one medoid changes, loop back
            not_stable += medoids[i] != new_medoid
            medoids[i] = new_medoid
    # in clusters : indices are vertices and values are cluster centers
    # memebership matrix : clusters are numbered in the order of their cluster ids
    # give ids to regions (based on order of their center's id), also return the mapping to get the centers
    mapping = dict(zip(np.unique(clusters), np.arange(len(medoids))))
    clusters = np.array([mapping[elt] for elt in clusters])
    # clusters [vertex_id (sorted):cluster_id]
    # mapping {cluster_id : centroid}
    return clusters, {v: k for k, v in mapping.items()}

def get_dijkstra_successive_hierarchial_regions(graph, region_numbers = [50, 200, 800]):
    
    # get the first level 
    original_clustering, mapping = dijkstra_geodesic_clustering(graph, region_numbers[0], fancy_init=True)
    divide_region_by = [region_numbers[i] // region_numbers[i-1] for i in range(1,len(region_numbers))]
    adj = np.transpose(np.nonzero(nx.adjacency_matrix(graph).todense()))
    adjs = [get_regions_adj(original_clustering, adj)]
    clusterings = [original_clustering]
    mappings = [mapping]
    parents = []
    geodesic_distances = []
    num_nodes = len(clusterings[-1])
    for i,div in enumerate(divide_region_by):
        # extract regions from previous clustering
        ordered_vertices = np.argsort(clusterings[-1])
        cluster_ids, counts = np.unique(clusterings[-1][ordered_vertices], return_counts=True)
        # utils
        previous_cpt = 0
        cluster_max_id_cpt = 0
        # containers
        step_clustering = np.ones(num_nodes).astype(int)
        step_parents = -np.ones(num_nodes)
        step_mapping = {}
        for i,cluster_id in enumerate(cluster_ids):
            # get region
            region = ordered_vertices[previous_cpt:previous_cpt+counts[i]]
            # if we can (card(region)>=div)
            if (len(region)>=div):
                # cluster region
                reindex = dict(zip(region, np.arange(len(region))))
                reverse_reindex = dict(zip(np.arange(len(region)), region))
                region_clustering, region_mapping = geodesic_clustering_medoids(nx.relabel_nodes(graph.subgraph(region).copy(), reindex), num_regions = div)
                for elt in region_mapping.keys():
                    step_mapping[elt+cluster_max_id_cpt] = reverse_reindex[region_mapping[elt]]
            else:
                # cluster region
                reindex = dict(zip(region, np.arange(len(region))))
                reverse_reindex = dict(zip(np.arange(len(region)), region))
                region_clustering, region_mapping = geodesic_clustering_medoids(nx.relabel_nodes(graph.subgraph(region).copy(), reindex), num_regions = len(region))
                for elt in region_mapping.keys():
                    step_mapping[elt+cluster_max_id_cpt] = reverse_reindex[region_mapping[elt]]
            # update cluster
            step_clustering[region] = region_clustering+cluster_max_id_cpt
            # update parents
            step_parents[region_clustering+cluster_max_id_cpt] = int(cluster_id)
            # update utils
            cluster_max_id_cpt = cluster_max_id_cpt+region_clustering.max()+1
            previous_cpt = previous_cpt+counts[i]
        adjs.append(get_regions_adj(step_clustering, adj))
        clusterings.append(step_clustering)
        parents.append(step_parents[step_parents>=0].astype(np.int))
        mappings.append(step_mapping)
    for i, mapping in enumerate(mappings):
        mappings[i] = np.array(list(mapping.values()))
        distances = np.zeros((mappings[i].shape[0], clusterings[0].shape[0]))
        for j in range(distances.shape[0]):
            distances[j] = dijkstra(graph, mappings[i][j], old_distance = None)
        geodesic_distances.append(distances)
    return clusterings, parents, mappings, adjs, geodesic_distances



def get_dijkstra_succesive_multilevel_regions(graphx, num_regions = None, fancy_init = False):
    # recepients
    geodesic_distanes = {}
    clusters_all = {}

    # step 1 : choose random node
    num_vertices = graphx.number_of_nodes()
    current_node = random.choice(np.arange(num_vertices))
    # step 2 : build U_n
    U_n = dijkstra(graphx, current_node, old_distance = None)
    if fancy_init:
        current_node = np.argmax(U_n)
        U_n = dijkstra(graphx, current_node, old_distance = None)
    geodesic_distanes[current_node] = U_n
    clusters = np.full((num_vertices), current_node)

    # the iteration
    previous = 1
    
    for j,num_region in enumerate(num_regions):
        for i in range(previous, num_region):
            # step 3 : choose farthest away node
            current_node = np.argmax(U_n)
            # step 4 : new map
            U_nn = dijkstra(graphx, current_node, old_distance = None)
            geodesic_distanes[current_node] = U_nn
            clusters[U_nn < U_n] = current_node
            U_n = np.minimum(U_n, U_nn)
        clusters_all[num_region] = np.copy(clusters)
        previous = num_region
    # in clusters : indices are vertices and values are cluster centers
    # memebership matrix : clusters are numbered in the order of their cluster ids
    # give ids to regions (based on order of their center's id), also return the mapping to get the centers
    cluster_centers = list(geodesic_distanes.keys())
    mapping = dict(zip(cluster_centers, np.arange(len(cluster_centers))))
    adjs = {}
    adj = np.transpose(np.nonzero(nx.adjacency_matrix(graphx).todense()))
    for k,v in clusters_all.items():
        clusters_all[k] = np.array([mapping[elt] for elt in v])
        adjs[k] = get_regions_adj(clusters_all[k], adj)
    
    distances = np.stack(list(geodesic_distanes.values()))
    # clusters_all {num_regions : [vertex_id (sorted):cluster_id]} 
    # mapping {cluster_id : centroid}
    # adjs {num_regions : adjs}
    # distances {num_regions : geo desitances from all centrs to other nodes}
    mapping = dict(zip(np.arange(len(cluster_centers)), cluster_centers))
    return clusters_all, mapping, adjs, distances

def get_dijkstra_regions(graph, region_numbers = [50, 200, 800]):
    
    # get the first level 
    original_clustering, mapping = dijkstra_geodesic_clustering(graph, region_numbers[0])
    divide_region_by = [region_numbers[i] // region_numbers[i-1] for i in range(1,len(region_numbers))]
    adj = np.transpose(np.nonzero(nx.adjacency_matrix(graph).todense()))
    adjs = [get_regions_adj(original_clustering, adj)]
    clusterings = [original_clustering]
    mappings = [mapping]
    parents = []
    num_nodes = len(clusterings[-1])
    for i,div in enumerate(divide_region_by):
        # extract regions from previous clustering
        ordered_vertices = np.argsort(clusterings[-1])
        cluster_ids, counts = np.unique(clusterings[-1][ordered_vertices], return_counts=True)
        # utils
        previous_cpt = 0
        cluster_max_id_cpt = 0
        # containers
        step_clustering = np.ones(num_nodes).astype(np.int)
        step_parents = -np.ones(num_nodes)
        step_mapping = {}
        for i,cluster_id in enumerate(cluster_ids):
            # get region
            region = ordered_vertices[previous_cpt:previous_cpt+counts[i]]
            # if we can (card(region)>=div)
            if (len(region)>=div):
                # cluster region
                reindex = dict(zip(region, np.arange(len(region))))
                reverse_reindex = dict(zip(np.arange(len(region)), region))
                region_clustering, region_mapping = dijkstra_geodesic_clustering(nx.relabel_nodes(graph.subgraph(region).copy(), reindex), div, fancy_init=True)
                for elt in region_mapping.keys():
                    step_mapping[elt+cluster_max_id_cpt] = reverse_reindex[region_mapping[elt]]
            else:
                # cluster region
                reindex = dict(zip(region, np.arange(len(region))))
                reverse_reindex = dict(zip(np.arange(len(region)), region))
                region_clustering, region_mapping = dijkstra_geodesic_clustering(nx.relabel_nodes(graph.subgraph(region).copy(), reindex), len(region), fancy_init=True)
                for elt in region_mapping.keys():
                    step_mapping[elt+cluster_max_id_cpt] = reverse_reindex[region_mapping[elt]]
            # update cluster
            step_clustering[region] = region_clustering+cluster_max_id_cpt
            # update parents
            step_parents[region_clustering+cluster_max_id_cpt] = int(cluster_id)
            # update utils
            cluster_max_id_cpt = cluster_max_id_cpt+region_clustering.max()+1
            previous_cpt = previous_cpt+counts[i]
        adjs.append(get_regions_adj(step_clustering, adj))
        clusterings.append(step_clustering)
        parents.append(step_parents[step_parents>=0].astype(np.int))
        mappings.append(step_mapping)
    return clusterings, parents, mappings, adjs

def get_regions_adj(clusters, adj):
    regions_adj = np.array([[],[]]).T
    # iterate over regions, for each region find it's neighbours
    ordered_vertices = np.argsort(clusters)
    clusters = np.stack([clusters[ordered_vertices], ordered_vertices], axis=-1)
    for i in range(len(np.unique(clusters[:,0]))):
        # get the region
        region = clusters[:,1][clusters[:,0] == i]
        # all frontier edges of this region (one vertex in one vertex out), candidates are edge indices
        candidates = np.where(np.isin(adj, region).sum(axis = -1) == 1)[0]        
        # all regions which have one of these edges is a neighbouring region
        selected_vertices = np.isin(clusters[:,1], np.unique(adj[candidates].flatten()))
        # just do a selection
        neighbours = np.unique(clusters[:,0][selected_vertices])
        neighbours = np.delete(neighbours, np.where(neighbours == i))
        regions_adj = np.concatenate([regions_adj,np.stack([np.full(neighbours.shape[0]+1,i),np.insert(neighbours, 0, i)], axis=1)], axis=0)
    return regions_adj.astype(np.int)
