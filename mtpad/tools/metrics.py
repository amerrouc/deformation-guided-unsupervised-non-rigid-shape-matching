import numpy as np
import scipy
from scipy import io
import os.path as osp
from pathlib import Path
import pickle


def get_closest_point(source_shape, target_shape):
    dists = scipy.spatial.distance.cdist(source_shape, target_shape, metric='euclidean')
    preds = np.argmin(dists, axis = -1)
    return preds

def load_geodist(filepath):
    data = io.loadmat(filepath)
    geodist = np.asarray(data['geodist'], dtype=np.float32)
    sqrt_area = data['sqrt_area'].toarray().flatten()[0]
    return geodist, sqrt_area

def evaluate_shape_pair(dataset_name, source_id, target_id, s_gt, t_gt, s_t_p, t_s_p, path_to_geodist, thresholds = np.linspace(0,1.0,100)):
    if dataset_name in ['faust_remeshed', 'faust_remeshed_anisotropic']:
        path_to_geodist_source = osp.join(path_to_geodist, 'tr_reg_{0:03d}.mat').format(source_id)
        geodist_src, sqrt_area_src = load_geodist(path_to_geodist_source)
        path_to_geodist_target = osp.join(path_to_geodist, 'tr_reg_{0:03d}.mat').format(target_id)
        geodist_trgt, sqrt_area_trgt = load_geodist(path_to_geodist_target)
        # from source to target
        s_t_m = np.stack([t_gt, s_t_p[s_gt]], axis=-1)
        acc_s_t = (s_t_m[:,0] == s_t_m[:,1]).mean()
        # s_t_m gives ground truth target nodes indexed by template, and predicted target nodes indexed by template
        s_t_m = np.ravel_multi_index(s_t_m.T, dims=[geodist_trgt.shape[0], geodist_trgt.shape[0]])
        geoerrs_s_t = (np.take(geodist_trgt, s_t_m) / sqrt_area_trgt)*100
        acc_radius_s_t = np.array([(geoerrs_s_t <= threshold).sum() / geoerrs_s_t.shape[0] for threshold in thresholds])*100
        # from target to source
        t_s_m = np.stack([s_gt, t_s_p[t_gt]], axis=-1)
        acc_t_s = (t_s_m[:,0] == t_s_m[:,1]).mean()
        # t_s_m gives ground truth source nodes indexed by template, and predicted source nodes indexed by template
        t_s_m = np.ravel_multi_index(t_s_m.T, dims=[geodist_src.shape[0], geodist_src.shape[0]])
        geoerrs_t_s = (np.take(geodist_src, t_s_m) / sqrt_area_src)*100
        acc_radius_t_s = np.array([(geoerrs_t_s <= threshold).sum() / geoerrs_t_s.shape[0] for threshold in thresholds])*100

        return np.mean(geoerrs_s_t), acc_s_t*100, acc_radius_s_t, np.mean(geoerrs_t_s), acc_t_s*100, acc_radius_t_s
    
    if dataset_name in ['scape_remeshed_aligned', 'scape_remeshed_anisotropic']:
        path_to_geodist_source = osp.join(path_to_geodist, 'mesh{0:03d}.mat').format(source_id)
        geodist_src, sqrt_area_src = load_geodist(path_to_geodist_source)
        path_to_geodist_target = osp.join(path_to_geodist, 'mesh{0:03d}.mat').format(target_id)
        geodist_trgt, sqrt_area_trgt = load_geodist(path_to_geodist_target)
        # from source to target
        s_t_m = np.stack([t_gt, s_t_p[s_gt]], axis=-1)
        acc_s_t = (s_t_m[:,0] == s_t_m[:,1]).mean()
        # s_t_m gives ground truth target nodes indexed by template, and predicted target nodes indexed by template
        s_t_m = np.ravel_multi_index(s_t_m.T, dims=[geodist_trgt.shape[0], geodist_trgt.shape[0]])
        geoerrs_s_t = (np.take(geodist_trgt, s_t_m) / sqrt_area_trgt)*100
        acc_radius_s_t = np.array([(geoerrs_s_t <= threshold).sum() / geoerrs_s_t.shape[0] for threshold in thresholds])*100
        # from target to source
        t_s_m = np.stack([s_gt, t_s_p[t_gt]], axis=-1)
        acc_t_s = (t_s_m[:,0] == t_s_m[:,1]).mean()
        # t_s_m gives ground truth source nodes indexed by template, and predicted source nodes indexed by template
        t_s_m = np.ravel_multi_index(t_s_m.T, dims=[geodist_src.shape[0], geodist_src.shape[0]])
        geoerrs_t_s = (np.take(geodist_src, t_s_m) / sqrt_area_src)*100
        acc_radius_t_s = np.array([(geoerrs_t_s <= threshold).sum() / geoerrs_t_s.shape[0] for threshold in thresholds])*100
        return np.mean(geoerrs_s_t), acc_s_t*100, acc_radius_s_t, np.mean(geoerrs_t_s), acc_t_s*100, acc_radius_t_s
    if dataset_name in ['shrec_remeshed']:
        path_to_geodist_source = osp.join(path_to_geodist, '{0:01d}.mat').format(source_id)
        geodist_src, sqrt_area_src = load_geodist(path_to_geodist_source)
        path_to_geodist_target = osp.join(path_to_geodist, '{0:01d}.mat').format(target_id)
        geodist_trgt, sqrt_area_trgt = load_geodist(path_to_geodist_target)

        # check for either ground truth maps
        path_to_gt = osp.join(path_to_geodist, 'SHREC_r_gt/groundtruth/', '{0:01d}_{1:01d}.map')
        if Path(path_to_gt.format(source_id, target_id)).exists(): 
            gt_s_t = np.loadtxt(Path(path_to_gt.format(source_id, target_id))).astype(int)-1
            t_gt = gt_s_t
            s_gt = s_gt
        elif Path(path_to_gt.format(target_id, source_id)).exists(): 
            gt_t_s = np.loadtxt(Path(path_to_gt.format(target_id, source_id))).astype(int)-1
            t_gt = t_gt
            s_gt = gt_t_s
        else:
            print("Error gt not found this is unexpected behaviour please check where things went wrong")
        # from source to target
        s_t_m = np.stack([t_gt, s_t_p[s_gt]], axis=-1)
        acc_s_t = (s_t_m[:,0] == s_t_m[:,1]).mean()
        # s_t_m gives ground truth target nodes indexed by template, and predicted target nodes indexed by template
        s_t_m = np.ravel_multi_index(s_t_m.T, dims=[geodist_trgt.shape[0], geodist_trgt.shape[0]])
        geoerrs_s_t = (np.take(geodist_trgt, s_t_m) / sqrt_area_trgt)*100
        acc_radius_s_t = np.array([(geoerrs_s_t <= threshold).sum() / geoerrs_s_t.shape[0] for threshold in thresholds])*100
        # from target to source
        t_s_m = np.stack([s_gt, t_s_p[t_gt]], axis=-1)
        acc_t_s = (t_s_m[:,0] == t_s_m[:,1]).mean()
        # t_s_m gives ground truth source nodes indexed by template, and predicted source nodes indexed by template
        t_s_m = np.ravel_multi_index(t_s_m.T, dims=[geodist_src.shape[0], geodist_src.shape[0]])
        geoerrs_t_s = (np.take(geodist_src, t_s_m) / sqrt_area_src)*100
        acc_radius_t_s = np.array([(geoerrs_t_s <= threshold).sum() / geoerrs_t_s.shape[0] for threshold in thresholds])*100
        return np.mean(geoerrs_s_t), acc_s_t*100, acc_radius_s_t, np.mean(geoerrs_t_s), acc_t_s*100, acc_radius_t_s
    if dataset_name in ['smal_remeshed']:
        path_to_id_name = osp.join(path_to_geodist, 'id_name_smal.pkl')
        with open(path_to_id_name, "rb") as fp:   # Unpickling
            id_name_smal = pickle.load(fp)
        # id to name
        path_to_geodist_source = osp.join(path_to_geodist, id_name_smal[source_id]+'.mat')
        geodist_src, sqrt_area_src = load_geodist(path_to_geodist_source)
        path_to_geodist_target = osp.join(path_to_geodist, id_name_smal[target_id]+'.mat')
        geodist_trgt, sqrt_area_trgt = load_geodist(path_to_geodist_target)
        # from source to target
        s_t_m = np.stack([t_gt, s_t_p[s_gt]], axis=-1)
        acc_s_t = (s_t_m[:,0] == s_t_m[:,1]).mean()
        # s_t_m gives ground truth target nodes indexed by template, and predicted target nodes indexed by template
        s_t_m = np.ravel_multi_index(s_t_m.T, dims=[geodist_trgt.shape[0], geodist_trgt.shape[0]])
        geoerrs_s_t = (np.take(geodist_trgt, s_t_m) / sqrt_area_trgt)*100
        acc_radius_s_t = np.array([(geoerrs_s_t <= threshold).sum() / geoerrs_s_t.shape[0] for threshold in thresholds])*100
        # from target to source
        t_s_m = np.stack([s_gt, t_s_p[t_gt]], axis=-1)
        acc_t_s = (t_s_m[:,0] == t_s_m[:,1]).mean()
        # t_s_m gives ground truth source nodes indexed by template, and predicted source nodes indexed by template
        t_s_m = np.ravel_multi_index(t_s_m.T, dims=[geodist_src.shape[0], geodist_src.shape[0]])
        geoerrs_t_s = (np.take(geodist_src, t_s_m) / sqrt_area_src)*100
        acc_radius_t_s = np.array([(geoerrs_t_s <= threshold).sum() / geoerrs_t_s.shape[0] for threshold in thresholds])*100
        return np.mean(geoerrs_s_t), acc_s_t*100, acc_radius_s_t, np.mean(geoerrs_t_s), acc_t_s*100, acc_radius_t_s
    if dataset_name in ["extended_faust"]:
        path_to_id_name = osp.join(path_to_geodist, 'id_name_extended_faust.pkl')
        with open(path_to_id_name, "rb") as fp:   # Unpickling
            id_name_extended_faust = pickle.load(fp)
        # id to name
        path_to_geodist_source = osp.join(path_to_geodist, id_name_extended_faust[source_id]+'.mat')
        geodist_src, sqrt_area_src = load_geodist(path_to_geodist_source)
        path_to_geodist_target = osp.join(path_to_geodist, id_name_extended_faust[target_id]+'.mat')
        geodist_trgt, sqrt_area_trgt = load_geodist(path_to_geodist_target)

        # from source to target
        s_t_m = np.stack([t_gt[s_t_p], s_gt], axis=-1)
        s_t_m = s_t_m[np.where((s_t_m == -1).sum(-1) == 0)[0]]
        acc_s_t = (s_t_m[:,0] == s_t_m[:,1]).mean()
        # s_t_m gives ground truth target nodes indexed by template, and predicted target nodes indexed by template
        s_t_m = np.ravel_multi_index(s_t_m.T, dims=[geodist_trgt.shape[0], geodist_trgt.shape[0]])
        geoerrs_s_t = (np.take(geodist_trgt, s_t_m) / sqrt_area_trgt)*100
        acc_radius_s_t = np.array([(geoerrs_s_t <= threshold).sum() / geoerrs_s_t.shape[0] for threshold in thresholds])*100
        # from target to source
        t_s_m = np.stack([s_gt[t_s_p], t_gt], axis=-1)
        t_s_m = t_s_m[np.where((t_s_m == -1).sum(-1) == 0)[0]]
        acc_t_s = (t_s_m[:,0] == t_s_m[:,1]).mean()
        # t_s_m gives ground truth source nodes indexed by template, and predicted source nodes indexed by template
        t_s_m = np.ravel_multi_index(t_s_m.T, dims=[geodist_src.shape[0], geodist_src.shape[0]])
        geoerrs_t_s = (np.take(geodist_src, t_s_m) / sqrt_area_src)*100
        acc_radius_t_s = np.array([(geoerrs_t_s <= threshold).sum() / geoerrs_t_s.shape[0] for threshold in thresholds])*100

        return np.mean(geoerrs_s_t), acc_s_t*100, acc_radius_s_t, np.mean(geoerrs_t_s), acc_t_s*100, acc_radius_t_s

#/softs/stow/matlab-2015b/bin/matlab  -nodesktop -nosplash -r "computeGeoDistMat_extended_faust_remeshed"