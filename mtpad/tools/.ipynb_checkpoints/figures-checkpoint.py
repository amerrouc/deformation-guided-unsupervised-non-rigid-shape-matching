import torch
import numpy as np
from matplotlib import cm, colors
import seaborn as sns
import matplotlib.pyplot as plt
import os.path as osp
import os
import sys
from .metrics import *
import trimesh
import cv2

def get_acc_vs_radius(errors_per_model, thresholds = np.linspace(0,0.1,100), save=True, writer_path=None, log=False, writer=None, epoch=None):
    if save or log:
        fig = plt.figure(figsize=(15, 10))
        plt.xlabel("% Diameter")
        plt.ylabel("% Correspondance")
        plt.title("Percentage of correct shape correspondences within a certain geodesic distance to the ground truth")
        plt.xlim(xmin=thresholds[0], xmax=thresholds[-1])  # this line
        plt.legend(loc='best')
    for model_name in errors_per_model:
        result, thresholds = get_curve(errors=errors_per_model[model_name], thresholds=thresholds)
        sns.set_style("whitegrid")
        if save or log:
            plt.plot(thresholds, result, label = model_name)
    if save:
        check = os.path.isdir(writer_path+'/acc_vs_radius')
        if not check:
            os.makedirs(writer_path+'/acc_vs_radius')
        plt.savefig(writer_path+'/acc_vs_radius'+'/accuracy_vs_radius.png')
    if log:
        fig.canvas.draw()
        data = np.fromstring(fig.canvas.tostring_rgb(), dtype=np.uint8, sep='')
        data = data.reshape(fig.canvas.get_width_height()[::-1] + (3,))
        writer.add_image(tag = '% Correspondance(% Diameter)', img_tensor = torch.tensor(data), global_step=epoch, dataformats='HWC')
        

def mesh_logger(epoch, geo_errors_per_node, test_loader, eval_shapes_ids, save, log, save_colors, writer, writer_path):
    # View/Save error on some meshes
    if log or save or save_colors:
        for shape_id in range(80,100):
            # get mesh and it's errors
            mesh = test_loader.dataset[shape_id-80]
            error = geo_errors_per_node[shape_id-80]
            # define color map
            color_map = cm.get_cmap('gist_heat_r')
            norm = colors.Normalize(vmin=0, vmax=10)
            color = np.array(color_map(norm(error)))[:,0:3]
            color *= 255.0/color.max()
            if log:
                # no dynamic logging (too heavy for tensorboard)
                writer.add_mesh('Test Mesh '+str(shape_id), vertices=mesh.pos.unsqueeze(0), colors=torch.tensor(color).unsqueeze(0), faces=mesh.face.T.unsqueeze(0), global_step=epoch)
            if save or save_colors:
                if save:
                    if shape_id in eval_shapes_ids:
                        # infer obj name and folder (a folder for each shape in the tensorboard directory)
                        path = osp.join(writer_path, 'error_shapes')
                        check = os.path.isdir(path)
                        if not check:
                            os.makedirs(path)
                        path = osp.join(path, 'shape_{0:03d}')
                        path = path.format(shape_id)
                        check = os.path.isdir(path)
                        if not check:
                            os.makedirs(path)
                        path = osp.join(path, 'epoch_{0:06d}.ply')   
                        write_ply(filename = path.format(epoch),points = mesh.pos.cpu().numpy(), faces = mesh.face.cpu().T.numpy(), colors = color)
                if save_colors:
                    if shape_id in eval_shapes_ids:
                        # infer obj name and folder (a folder for each shape in the tensorboard directory)
                        path = osp.join(writer_path, 'error_colors')
                        check = os.path.isdir(path)
                        if not check:
                            os.makedirs(path)
                        path = osp.join(path, 'shape_{0:03d}')
                        path = path.format(shape_id)
                        check = os.path.isdir(path)
                        if not check:
                            os.makedirs(path)
                        path = osp.join(path, 'epoch_{0:06d}.npy')   
                        np.save(file=path.format(epoch), arr=color)
                        
def generate_images_from_mesh_and_colors(shape_path, colors_path, range_for_colors, save_path, resolution = (512,512)):
    # shape path : full path to shape
    # colors_path :  path to be formated with values in : range_for_colors
    # save path : where to save the images (image name will be mesh_image_range_for_colors[i].png)
    mesh = trimesh.load(shape_path, process=False)
    for i in range_for_colors:
        colors = np.load(colors_path.format(i))
        try:
            mesh.visual.vertex_colors = colors
            scene = mesh.scene()
            # increment the file name
            file_name = save_path+'/mesh_image_' + str(i) + '.png'
            # save a render of the object as a png
            png = scene.save_image(resolution=resolution, visible=True)
            with open(file_name, 'wb') as f:
                f.write(png)
                f.close()

        except BaseException as E:
            print("unable to save image", str(E))
            
def generate_video(img_folder_paths, img_folder_paths_range, num_images = 3, num_images_per_frame=(1,1), save_video_path='./video.avi'):
    # img_folder_paths : path to different folders to be formated usng values in range : img_folder_paths_range
    # num_images : how many images to use in each folder (same for all)
    # num_images_per_frame : how many images in each video frame
    # video name : (name of the video)
    images_super = []
    for folder_id in img_folder_paths_range:
        images_super.append([img for img in os.listdir(img_folder_paths.format(folder_id)) if img.endswith(".png")])
    h,w,_ = cv2.imread(os.path.join(img_folder_paths.format(img_folder_paths_range[0]), images_super[0][0])).shape
    video = cv2.VideoWriter(save_video_path, 0, 1, (num_images_per_frame[0]*h, num_images_per_frame[1]*w))
    for i in range(num_images):
        images_to_cat = []
        for j,folder_id in enumerate(img_folder_paths_range):
            images_to_cat.append(cv2.imread(os.path.join(img_folder_paths.format(folder_id), images_super[j][i])))
        assert(len(images_to_cat) == num_images_per_frame[0]*num_images_per_frame[1])
        images_to_cat = np.vstack([np.hstack(images_to_cat[i*num_images_per_frame[1]:(i+1)*num_images_per_frame[1]]) for i in range(num_images_per_frame[0])])
        video.write(images_to_cat)