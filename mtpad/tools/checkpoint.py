from pathlib import Path
import torch.nn.functional as F
from typing import List
import torch
SEED = 1234
torch.manual_seed(SEED)
torch.backends.cudnn.deterministic = True

        
class CheckpointStateTwoOptimizers():
    """A model checkpoint state."""
    def __init__(self, model, optimizer=None, scheduler=None, optimizer_association=None, scheduler_association=None, optimizer_deformation=None, scheduler_deformation=None,\
        epoch=0, writer_path='./writer', savepath='./checkpt.pt'):

        self.model = model
        self.optimizer = optimizer
        self.scheduler = scheduler
        self.optimizer_association = optimizer_association
        self.scheduler_association = scheduler_association
        self.optimizer_deformation = optimizer_deformation
        self.scheduler_deformation = scheduler_deformation
        self.epoch = epoch
        self.savepath = Path(savepath)
        self.writer_path = writer_path

    def state_dict(self):
        """Checkpoint's state dict, to save and load"""
        dict_ = dict()
        dict_['model'] = self.model.state_dict()
        if self.optimizer:
            dict_['optimizer'] = self.optimizer.state_dict()
        if self.scheduler:
            dict_['scheduler'] = self.scheduler.state_dict()
        if self.optimizer_deformation:
            dict_['optimizer_deformation'] = self.optimizer_deformation.state_dict()
        if self.optimizer_association:
            dict_['optimizer_association'] = self.optimizer_association.state_dict()
        if self.scheduler_association:
            dict_['scheduler_association'] = self.scheduler_association.state_dict()
        if self.scheduler_deformation:
            dict_['scheduler_deformation'] = self.scheduler_deformation.state_dict()
        dict_['epoch'] = self.epoch
        dict_["writer_path"] = self.writer_path
        return dict_

    def save(self, suffix=''):
        """Serializes the checkpoint.
        Args:
            suffix (str): if provided, a suffix will be prepended before the extension
                of the object's savepath attribute.
        """
        if suffix:
            savepath = self.savepath.parent / Path(self.savepath.stem + suffix + self.savepath.suffix)
        else:
            savepath = self.savepath
        with savepath.open('wb') as fp:
            torch.save(self.state_dict(), fp)

    def load(self):
        """Deserializes and map the checkpoint to the available device."""
        if self.savepath.is_file():
            with self.savepath.open('rb') as fp:
                state_dict = torch.load(
                    fp, map_location=torch.device('cuda' if torch.cuda.is_available() else 'cpu'))
                self.update(state_dict)
        else:
            self.save()

    def update(self, state_dict):
        """Updates the object with a dictionary
        Args:
            state_dict (dict): a dictionary with keys:
                - 'model' containing a state dict for the checkpoint's model
                - 'optimizer' containing a state for the checkpoint's optimizer (optional)
                - 'epoch' containing the associated epoch number
        """
        self.model.load_state_dict(state_dict['model'])
        if self.optimizer is not None and 'optimizer' in state_dict:
            self.optimizer.load_state_dict(state_dict['optimizer'])
        if self.scheduler is not None and 'scheduler' in state_dict:
            self.scheduler.load_state_dict(state_dict['scheduler'])

        if self.optimizer_association is not None and 'optimizer_association' in state_dict:
            self.optimizer_association.load_state_dict(state_dict['optimizer_association'])
        if self.scheduler_association is not None and 'scheduler_association' in state_dict:
            self.scheduler_association.load_state_dict(state_dict['scheduler_association'])

        if self.optimizer_deformation is not None and 'optimizer_deformation' in state_dict:
            self.optimizer_deformation.load_state_dict(state_dict['optimizer_deformation'])
        if self.scheduler_deformation is not None and 'scheduler_deformation' in state_dict:
            self.scheduler_deformation.load_state_dict(state_dict['scheduler_deformation'])
            
        self.epoch = state_dict['epoch']
        if self.epoch > 0:
            self.writer_path = state_dict['writer_path']
        