from .dataset_transforms import *
from .dataset import *
from torch_geometric.datasets import *
from torch_geometric.transforms import *
import torch_geometric
import torch
import itertools
from math import comb



class AllPairsSamplerUnordered(torch.utils.data.Sampler):
    def __init__(self, data_source, shuffle=True):
        self.data_source = data_source  
        self.len = comb(len(data_source), 2)
        self.shuffle = shuffle      
    def __iter__(self):
        indices = range(len(self.data_source))
        paired_indices = torch.tensor(list(itertools.combinations(indices, 2)))
        paired_indices = torch.stack([paired_indices[i] for i in range(len(paired_indices)) if not paired_indices[i][0] == paired_indices[i][1]])
        if self.shuffle:
            paired_indices = paired_indices[torch.randperm(len(paired_indices))]
        indices = paired_indices.view(-1)
        return iter(indices.tolist())
    
    def __len__(self):
        return 2*self.len


class SamplePosePairsSamplerUnordered(torch.utils.data.Sampler):
    def __init__(self, data_source, shuffle=True):
        self.data_source = data_source  
        self.len = self.get_different_pose_sample_index_pairs(data_source).shape[0]
        self.shuffle = shuffle      
    def __iter__(self):
        paired_indices = self.get_different_pose_sample_index_pairs(self.data_source)
        if self.shuffle:
            paired_indices = paired_indices[torch.randperm(len(paired_indices))]
        indices = paired_indices.view(-1)
        return iter(indices.tolist())
    def get_different_pose_sample_index_pairs(self, data_source):
        def get_a_sample_from_each_pose(pose_indices):
            unique_poses = np.unique(pose_indices)
            list_of_indices = [np.random.choice(np.where(pose_indices==p_id)[0]) for p_id in unique_poses]
            return list_of_indices 
        indices = range(len(data_source))
        pose_indices = np.array([data_source[ind_p].pose_id.item()for ind_p in indices])
        list_of_indices = get_a_sample_from_each_pose(pose_indices)
        paired_indices = torch.tensor(list(itertools.combinations(list_of_indices, 2)))
        paired_indices = torch.stack([paired_indices[i] for i in range(len(paired_indices)) if not paired_indices[i][0] == paired_indices[i][1]])
        paired_indices = paired_indices
        return paired_indices
    
    def __len__(self):
        return 2*self.len



def get_scape_remeshed_aligned(batch_size=1, data_path='../../data/scape_remeshed_aligned_mtpad_for_training/', transform=None, mode='hops'):
    if mode=='primal_patches_3':
        data_path += 'primal_patches_3/'
        list_pre_transfroms = [FaceToEdge(False), GetPatchMultiLevel(num_regions = [50, 200, 800], mode='dijkstra_vertices'), CenterScale(), GetFeatures(targets = ['vertex_normals']), GetPatchMultiLevelUtils(), GeodesicGaussianPatchWeights(n = 1/2)]
        pre_transform = Compose(list_pre_transfroms)
    if mode=='primal_patches_4':
        data_path += 'primal_patches_4/'
        list_pre_transfroms = [FaceToEdge(False), GetPatchMultiLevel(num_regions = [50, 200, 800], mode='dijkstra_vertices'), CenterScale(), GetFeatures(targets = ['vertex_normals']), GetPatchMultiLevelUtils(), GeodesicGaussianPatchWeights(n = 1/2)]
        pre_transform = Compose(list_pre_transfroms)
    if mode=='primal_patches_5':
        data_path += 'primal_patches_5/'
        list_pre_transfroms = [FaceToEdge(False), GetPatchMultiLevel(num_regions = [50, 200, 800], mode='dijkstra_vertices'), CenterScale(),\
            GetFeatures(targets = ['vertex_normals']), GetPatchMultiLevelUtils(), GeodesicGaussianPatchWeights(n = 1/2), GetVertexLevelUtils(path_to_matrix = osp.join("../data/scape_remeshed_aligned_mtpad_for_training/shapes_distances_matrices/geodist", 'mesh{0:03d}.mat'))]
        pre_transform = Compose(list_pre_transfroms)
    if mode=='primal_patches_7':
        data_path += 'primal_patches_7/'
        list_pre_transfroms = [FaceToEdge(False), GetPatchMultiLevel(num_regions = [800], mode='dijkstra_vertices'), CenterScale(),\
            GetFeatures(targets = ['vertex_normals']), GetPatchMultiLevelUtils(), GeodesicGaussianPatchWeights(n = 1/2), GetVertexLevelUtils(path_to_matrix = osp.join("../data/scape_remeshed_aligned_mtpad_for_training/shapes_distances_matrices/geodist", 'mesh{0:03d}.mat'))]
        pre_transform = Compose(list_pre_transfroms)
        
    train_set = ScapeAligned(root=data_path, pre_transform=pre_transform, transform=transform)
    test_set = ScapeAligned(root=data_path, pre_transform=pre_transform, transform=transform, train=False)

    # not the challenging setup
    shapes_train = list(range(0,45))
    shapes_val = list(range(45,51))
    shapes_test = list(range(51,72))

    train = train_set[shapes_train]
    val = train_set[shapes_val]
    test = test_set

    sampler_train = AllPairsSamplerUnordered(train, shuffle=True)
    sampler_val = AllPairsSamplerUnordered(val, shuffle=False)
    sampler_test = AllPairsSamplerUnordered(test, shuffle=False)
    
    
    # dataloaders
    loader_train = torch_geometric.loader.DataListLoader(train, batch_size=batch_size, sampler=sampler_train)
    loader_val = torch_geometric.loader.DataListLoader(val, batch_size=batch_size, sampler=sampler_val)
    loader_test = torch_geometric.loader.DataListLoader(test, batch_size=batch_size, sampler=sampler_test) 
    
    return loader_train, loader_val, loader_test


def get_extended_faust_remeshed(batch_size=1, data_path='../../data/extended_faust_mtpad_for_training/', transform=None, mode='hops', sampler='diff'):
    if mode=='primal_patches_3':
        data_path += 'primal_patches_3/'
        list_pre_transfroms = [FaceToEdge(False), GetPatchMultiLevel(num_regions = [50, 200, 800], mode='dijkstra_vertices'), CenterScale(), GetFeatures(targets = ['vertex_normals']), GetPatchMultiLevelUtils(), GeodesicGaussianPatchWeights(n = 1/2)]
        pre_transform = Compose(list_pre_transfroms)
    if mode=='primal_patches_4':
        data_path += 'primal_patches_4/'
        list_pre_transfroms = [FaceToEdge(False), GetPatchMultiLevel(num_regions = [50, 200, 800], mode='dijkstra_vertices'), CenterScale(), GetFeatures(targets = ['vertex_normals']), GetPatchMultiLevelUtils(), GeodesicGaussianPatchWeights(n = 1/2)]
        pre_transform = Compose(list_pre_transfroms)
    if mode=='primal_patches_5':
        data_path += 'primal_patches_5/'
        list_pre_transfroms = [FaceToEdge(False), GetPatchMultiLevel(num_regions = [50, 200, 800], mode='dijkstra_vertices'), CenterScale(),\
            GetFeatures(targets = ['vertex_normals']), GetPatchMultiLevelUtils(), GeodesicGaussianPatchWeights(n = 1/2), GetVertexLevelUtils(path_to_matrix = None)]
        pre_transform = Compose(list_pre_transfroms)
    if mode=='primal_patches_7':
        data_path += 'primal_patches_7/'
        list_pre_transfroms = [FaceToEdge(False), GetPatchMultiLevel(num_regions = [800], mode='dijkstra_vertices'), CenterScale(),\
            GetFeatures(targets = ['vertex_normals']), GetPatchMultiLevelUtils(), GeodesicGaussianPatchWeights(n = 1/2), GetVertexLevelUtils(path_to_matrix =None)]
        pre_transform = Compose(list_pre_transfroms)
    if mode=='primal_patches_8':
        data_path += 'primal_patches_8/'
        list_pre_transfroms = [FaceToEdge(False), GetPatchMultiLevel(num_regions = [200, 800], mode='dijkstra_vertices'), CenterScale(),\
            GetFeatures(targets = ['vertex_normals']), GetPatchMultiLevelUtils(), GeodesicGaussianPatchWeights(n = 1/2), GetVertexLevelUtils(path_to_matrix =None)]
        pre_transform = Compose(list_pre_transfroms)
    
    if mode=='primal_patches_9':
        data_path += 'primal_patches_9/'
        list_pre_transfroms = [FaceToEdge(False), GetPatchMultiLevel(num_regions = [25, 50, 200, 800], mode='dijkstra_vertices'), CenterScale(),\
            GetFeatures(targets = ['vertex_normals']), GetPatchMultiLevelUtils(), GeodesicGaussianPatchWeights(n = 1/2), GetVertexLevelUtils(path_to_matrix =None)]
        pre_transform = Compose(list_pre_transfroms)
    
        
    train_set = Extended_faust_aligned_remeshed(root=data_path, pre_transform=pre_transform, transform=transform)
    test_set = Extended_faust_aligned_remeshed(root=data_path, pre_transform=pre_transform, transform=transform, train=False)



    train = train_set[:392]
    val = train_set[392:]
    test = test_set
    if sampler == 'same':
        sampler_train = AllPairsSamplerUnordered(train, shuffle=True)
        sampler_val = AllPairsSamplerUnordered(val, shuffle=False)
        sampler_test = AllPairsSamplerUnordered(test, shuffle=False)
    if sampler == 'diff':
        sampler_train = SamplePosePairsSamplerUnordered(train, shuffle=True)
        sampler_val = SamplePosePairsSamplerUnordered(val, shuffle=False)
        sampler_test = SamplePosePairsSamplerUnordered(test, shuffle=False)
    
    
    # dataloaders
    loader_train = torch_geometric.loader.DataListLoader(train, batch_size=batch_size, sampler=sampler_train)
    loader_val = torch_geometric.loader.DataListLoader(val, batch_size=batch_size, sampler=sampler_val)
    loader_test = torch_geometric.loader.DataListLoader(test, batch_size=batch_size, sampler=sampler_test) 
    
    return loader_train, loader_val, loader_test

