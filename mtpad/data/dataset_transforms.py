import torch_geometric
import torch
# just add the parent folder to be accessible
from mtpad.tools import *
import torch
import mtpad.tools as mtpadtools 
import math
import numbers
from torch_geometric.transforms import BaseTransform



# begin : those being used
class GetPatchMultiLevel(object):
    def __init__(self, num_regions = [50, 200, 800], mode='dijkstra_vertices', return_dist_matrices = True):
        self.num_regions = num_regions
        self.mode = mode
        self.return_dist_matrices = return_dist_matrices
    def get_data_as_distance_graph(self, data, mode='vertices'):
        if mode == 'vertices':
            # build graph from mesh (nodes as vertices, eucliean distance as weight on edges)
            vertices = data.pos.numpy()
            adj = data.edge_index.T.numpy()
            graph = nx.Graph()
            graph.add_nodes_from(np.arange(vertices.shape[0]))
            graph.add_edges_from(adj)
            # adding edge weights (l2 distance between points)
            keys = [tuple(i) for i in adj]
            values = list(np.sqrt(((vertices[adj[:,0]] - vertices[adj[:,1]]) ** 2).sum(axis = -1)))
            nx.set_edge_attributes(graph, values = dict(zip(keys, values)), name = 'weight')
            return graph
    def __call__(self, data):
        if self.mode == 'dijkstra_vertices':
            mesh_graph  = self.get_data_as_distance_graph(data, mode='vertices')
            # get the clusters
            clusters_all, mapping, adjs, distances  = mtpadtools.get_dijkstra_succesive_multilevel_regions(mesh_graph, num_regions = self.num_regions)
            mapping_regions = {r : j+1 for j, r in enumerate(reversed(self.num_regions))}
            data.clusters = [{mapping_regions[k] : torch.tensor(v) for k, v in clusters_all.items()}]
            data.region_adjs = [{mapping_regions[i]: torch.tensor(ddjj).T for i,ddjj in adjs.items()}]
            data.distances = [{0: torch.tensor(distances)}]
            data.mapping = [mapping]
        return data

class CenterScale(BaseTransform):
    def __init__(self):
        self.reference_diameter = 1.0
    def center(self, pos):
        centroid = torch.mean(pos, dim = 0, keepdim = True)
        return pos - centroid
    def scale(self, pos, diameter):
        return (self.reference_diameter / diameter) * pos
    def __call__(self, data):
        data.pos = self.center(self.scale(data.pos, data.distances[0][0].max().item()))
        return data


class GetFeatures(object):
    def __init__(self, targets = ['vertex_normals']):
        self.targets = targets
    def __call__(self, data):
        trimesh_data = torch_geometric.utils.to_trimesh(data)
        if 'vertex_normals' in self.targets:
            data.vertex_normals = torch.tensor(trimesh_data.vertex_normals)
        return data

class GetPatchMultiLevelUtils(object):
    def __init__(self):
        pass
    def get_list_adj(self, edge_index):
        node_indices, num_neighbours = torch.unique(edge_index.T[:,0], return_counts=True)
        K = torch.max(num_neighbours).item()
        neighbours = torch.split_with_sizes(edge_index.T[:,1], tuple(num_neighbours))
        adj = torch.stack([torch.cat([i, i.new_full((K - i.size(0),), -1)], 0) for i in neighbours],1).T
        mask = adj == -1
        adj[adj == -1] = 0
        return adj, mask
    def __call__(self, data):
        # cluster numbers are in decreassing order (example [50,200,800]=[3,2,1])
        # get list adjs and masks (requires region adjs in edge ndex format before)

        # make geodesic distances fraction of diameter
        data.distances[0][0] = data.distances[0][0]/data.distances[0][0].max().item()
        data.region_list_adjs = [{k: self.get_list_adj(v) for k,v in data.region_adjs[0].items()}]
        # mapping
        data.mapping = torch.tensor(list(data.mapping[0].values()))
        # cluster sizes
        data.cluster_sizes =  [{k : v.max()+1 for k,v in data.clusters[0].items()}]
        # restricted geodesic distances
        data.cluster_distances = [{i: data.distances[0][0][torch.arange(c_num),:].T[data.mapping[torch.arange(c_num)],:].T for i, c_num in data.cluster_sizes[0].items()}]
        return data       

class GeodesicGaussianPatchWeights(object):
    def __init__(self, n=1/2):
        self.n = n
    def __call__(self, data):
        point_to_cluster_weights = {}
        # geodesic distance from centers to all (centers don't change)
        # shape num_clusters_max ,num_points
        dist_centers_vs_all = data.distances[0][0].numpy()
        for i in data.clusters[0]:
            # extract cluster centers:
            # shape : num_points
            c = data.clusters[0][i]
            # shape:  num clusters
            center_ids = torch.unique(c).numpy()
            # shape num_points,num_clusters
            mask = np.zeros((c.shape[0], c.max()+1))
            # shape num_points,num_clusters (indicator where cluster)
            mask[np.arange(c.shape[0]),c] = 1
            # shape num_clusters, num_points
            dists = dist_centers_vs_all[center_ids]
            # shape num_points, num_clusters
            dists_point_to_cluster = dist_centers_vs_all[center_ids].T
            # shape num_cluster, num_points : for each cluster distance of it's points to its center
            dists[~mask.T.astype(np.bool_)] = 0.0
            # shape num_clusters, 1 : for each cluster maximum distance from its center to it's memebers (will serve as std for the gaussian centered around him)
            stds = (self.n*np.expand_dims(dists.max(axis=-1), axis=-1)) + 1e-5

            # for each point gives std of neighbours of it's cluster (including it's cluster)
            point_to_cluster_std = np.take_along_axis(stds, data.region_list_adjs[0][i][0].numpy(), axis=0)[c]
            # point to cluster distance (restircted to neighbours of point's cluster)
            point_to_cluster_distance = np.take_along_axis(dists_point_to_cluster, data.region_list_adjs[0][i][0][c].numpy(), axis=-1)
            geodesic_gaussian = torch.tensor((1/(point_to_cluster_std*np.sqrt(2*np.pi)))*np.exp(-(1/2)*((point_to_cluster_distance**2)/((point_to_cluster_std)**2))))
            geodesic_gaussian = geodesic_gaussian.masked_fill(data.region_list_adjs[0][i][1][c], 0.0)
            weights = geodesic_gaussian / geodesic_gaussian.sum(dim=-1, keepdim=True)
            point_to_cluster_weights[i] = weights
        data.point_to_cluster_weights = [point_to_cluster_weights]
        return data

class GetVertexLevelUtils(object):
    def __init__(self, n = 1/2, path_to_matrix = None):
        self.n = n
        self.path_to_matrix = path_to_matrix
    def get_list_adj(self, edge_index):
        node_indices, num_neighbours = torch.unique(edge_index.T[:,0], return_counts=True)
        # add self loops
        K = torch.max(num_neighbours).item()+1
        neighbours = torch.split_with_sizes(edge_index.T[:,1], tuple(num_neighbours))
        adj = torch.stack([torch.cat([torch.cat([torch.tensor([nn]), i], dim=0), i.new_full((K - (i.size(0)+1),), -1)], 0) for nn,i in enumerate(neighbours)],1).T
        mask = adj == -1
        adj[adj == -1] = 0
        return adj, mask
    def __call__(self, data):
        # have to add geodesic distance matrix
        if self.path_to_matrix is None:
            # place holder, for larger mashes this is not possible to compute
            data.cluster_distances[0][0] = torch.zeros((5,5))
        else:
            geodist = io.loadmat(self.path_to_matrix.format(data.id.item()))
            geodist = np.asarray(geodist['geodist'], dtype=np.float32)
            data.cluster_distances[0][0] = torch.tensor(geodist / geodist.max())
        # have to add list adj
        data.region_list_adjs[0][0] = self.get_list_adj(data.edge_index)
        # have to add the weights

        # how to
        pos_3d = data.pos.numpy()
        # so select neighbours
        point_to_neighs_coordinates = pos_3d[data.region_list_adjs[0][0][0].numpy()]
        # compute euclidean distance for neighbours and store
        point_to_neighs_dists = np.sqrt(((np.expand_dims(pos_3d, axis=1) - point_to_neighs_coordinates)**2).sum(-1))
        # then compute stds
        point_to_neighs_dists[data.region_list_adjs[0][0][1]] = 0.0
        point_std = np.max(point_to_neighs_dists, axis=-1)
        point_to_neigh_stds = point_std[data.region_list_adjs[0][0][0].numpy()]
        euclidean_gaussian = torch.tensor((1/(point_to_neigh_stds*np.sqrt(2*np.pi)))*np.exp(-(1/2)*((point_to_neighs_dists**2)/((point_to_neigh_stds)**2))))

        euclidean_gaussian = euclidean_gaussian.masked_fill(data.region_list_adjs[0][0][1], 0.0)
        weights = euclidean_gaussian / euclidean_gaussian.sum(dim=-1, keepdim=True)
        data.point_to_cluster_weights[0][0] = weights
        return data  

# eof those being used

