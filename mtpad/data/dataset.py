from torch_geometric.data import InMemoryDataset, extract_zip
from torch_geometric.io import read_ply
from typing import Optional, Callable, List
import numpy as np
import os.path as osp
import os
import progressbar
import shutil
import torch

import os.path as osp
import shutil
from typing import Callable, List, Optional

import torch

from torch_geometric.data import InMemoryDataset, extract_zip
from torch_geometric.io import read_ply, read_off, read_obj
from joblib import Parallel, delayed, cpu_count
import pickle





class ScapeAligned(InMemoryDataset):
    url = 'https://github.com/llorz/SGA18_orientation_BCICP_dataset'

    def __init__(self, root: str, train: bool = True,
                 transform: Optional[Callable] = None,
                 pre_transform: Optional[Callable] = None,
                 pre_filter: Optional[Callable] = None):
        super().__init__(root, transform, pre_transform, pre_filter)
        path = self.processed_paths[0] if train else self.processed_paths[1]
        self.data, self.slices = torch.load(path)

    @property
    def raw_file_names(self) -> str:
        return 'SCAPE_REMESH_ALIGN.zip'

    @property
    def processed_file_names(self) -> List[str]:
        return ['training.pt', 'test.pt']

    def download(self):
        raise RuntimeError(
            f"Dataset not found. Please download '{self.raw_file_names}' (FAUST folder in the url zipped) from "
            f"'{self.url}' and move it to '{self.raw_dir}'")
    def read_coress(self, path):
        f=open(path,"r")
        lines=f.readlines()
        result=[]
        for x in lines:
            result.append(int(x.split("\n")[0]))
        f.close()
        return result

    def process(self):
        extract_zip(self.raw_paths[0], self.raw_dir, log=False)
        # where the meshes are stored
        mesh_path = osp.join(self.raw_dir,'SCAPE_REMESH_ALIGN', 'shapes')
        mesh_path = osp.join(mesh_path, 'mesh{0:03d}.off')
        # where the ground truth correspondence is stored
        coress_path = osp.join(self.raw_dir,'SCAPE_REMESH_ALIGN', "corres")
        coress_path = osp.join(coress_path, 'mesh{0:03d}.vts')
        data_list = []
        for i in range(72):
            if i != 51:
                data = read_off(mesh_path.format(i))
                # from 1 to we change to 0 to
                data.labels = torch.tensor(self.read_coress(coress_path.format(i)))-1
                data.id = torch.tensor([i], dtype=torch.long)
                if self.pre_filter is not None and not self.pre_filter(data):
                    continue
                if self.pre_transform is not None:
                    data = self.pre_transform(data)
                data_list.append(data)
                print("done with example : ", i)

        torch.save(self.collate(data_list[:51]), self.processed_paths[0])
        torch.save(self.collate(data_list[51:]), self.processed_paths[1])

        shutil.rmtree(osp.join(self.raw_dir, 'SCAPE_REMESH_ALIGN'))



# extended faust

class Extended_faust_aligned_remeshed(InMemoryDataset):
    def __init__(self, root: str, train: bool = True,
                transform: Optional[Callable] = None,
                pre_transform: Optional[Callable] = None,
                pre_filter: Optional[Callable] = None):
        super().__init__(root, transform, pre_transform, pre_filter)
        path = self.processed_paths[0] if train else self.processed_paths[1]
        self.data, self.slices = torch.load(path)

    @property
    def raw_file_names(self) -> str:
        return 'ExtFaust_remeshed.zip'

    @property
    def processed_file_names(self) -> List[str]:
        return ['training.pt', 'test.pt']

    def download(self):
        raise RuntimeError(
            'Dataset not found.')
    def process(self):
        def get_num_batch(num_cpus, num_jobs):
            div = num_jobs // num_cpus
            if div == 0:
                return 1
            else:
                if ((num_jobs - (num_cpus*div))>0):
                    return div+1
                else:
                    return div
        # extract the zip 
        # /shapes contains meshes in .off format and /labels contains vertex to template faces correspondence (face ID)
        extract_zip(self.raw_paths[0], self.raw_dir, log=False)
        mesh_path = osp.join(self.raw_dir, 'ExtFaust_remeshed', 'shapes')
        gt_path = osp.join(self.raw_dir, 'ExtFaust_remeshed', 'gt_curated')
        train_val_test_split_path = osp.join(self.raw_dir, 'ExtFaust_remeshed', 'split', 'train_val_test.pkl')
        with open(train_val_test_split_path, 'rb') as f:
            train_val_test_split = pickle.load(f)
        data_list_intermediate = []
        data_list = []
        id_train = []
        id_val = []
        id_test = []
        for i,file in enumerate(os.listdir(os.fsencode(mesh_path))):
            file_name = os.fsdecode(file)
            if file_name.split('.')[0] in train_val_test_split['train']:
                id_train.append(i)
            if file_name.split('.')[0] in train_val_test_split['val']:
                id_val.append(i)
            if file_name.split('.')[0] in train_val_test_split['test']:
                id_test.append(i)
            shape_id = int(file_name.split('_')[1])
            pose_id = int(file_name.split('_')[3].split('.')[0])
            data = read_off(osp.join(mesh_path, file_name))
            data.labels = torch.tensor(np.genfromtxt(osp.join(gt_path, file_name.split('.')[0]+('.out')), delimiter=',').astype(np.int_))
            data.id = torch.tensor([i], dtype=torch.long)
            data.pose_id = torch.tensor([pose_id], dtype=torch.long)
            data.shape_id = torch.tensor([shape_id], dtype=torch.long)
            if self.pre_filter is not None and not self.pre_filter(data):
                continue
            data_list_intermediate.append(data)
        print("Finished step 1")
        print(len(data_list_intermediate))
        if self.pre_transform is not None:
            data_list = Parallel(n_jobs=-1, batch_size=get_num_batch(cpu_count(), len(data_list_intermediate)), verbose=4)(
            delayed(self.pre_transform)(data_list_intermediate[k])
            for k in range(len(data_list_intermediate)))
        data_list_train = [data_list[j] for j in id_train]+[data_list[j] for j in id_val]
        data_list_test = [data_list[j] for j in id_test]
        torch.save(self.collate(data_list_train), self.processed_paths[0])
        # place holder
        torch.save(self.collate(data_list_test), self.processed_paths[1])
        
        shutil.rmtree(osp.join(self.raw_dir, 'ExtFaust_remeshed'))


